# RUYI 包管理 Container RevyOS riscv64 测试结果

本次测试基于 RUYI 0.6.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.6.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 systemd-nspawn RevyOS 20231210 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ RevyOS 镜像使用当前最新版本 [20231210](https://mirror.iscas.ac.cn/revyos/extra/images/lpi4a/20231210/)
+ RevyOS 容器测试环境为在 RevyOS 20231210 上搭建的 systemd-nspawn 容器，预装软件与 20231210 镜像看齐

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Thu Mar 14 05:18:33 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Thu Mar 14 05:22:02 2024 - INFO  - The case exit by code 0.
Thu Mar 14 05:22:03 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Thu Mar 14 05:22:03 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Thu Mar 14 05:26:56 2024 - INFO  - The case exit by code 0.
Thu Mar 14 05:26:56 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Thu Mar 14 05:26:57 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu Mar 14 05:27:57 2024 - INFO  - The case exit by code 0.
Thu Mar 14 05:27:57 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu Mar 14 05:27:58 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Thu Mar 14 05:39:42 2024 - INFO  - The case exit by code 0.
Thu Mar 14 05:39:42 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Thu Mar 14 05:39:43 2024 - INFO  - start to run testcase:ruyi_test_common.
Thu Mar 14 05:56:59 2024 - INFO  - The case exit by code 0.
Thu Mar 14 05:56:59 2024 - INFO  - End to run testcase:ruyi_test_common.
Thu Mar 14 05:57:00 2024 - INFO  - start to run testcase:ruyi_test_config.
Thu Mar 14 05:59:30 2024 - INFO  - The case exit by code 0.
Thu Mar 14 05:59:30 2024 - INFO  - End to run testcase:ruyi_test_config.
Thu Mar 14 05:59:30 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Thu Mar 14 06:01:27 2024 - INFO  - The case exit by code 0.
Thu Mar 14 06:01:27 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Thu Mar 14 06:01:27 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Thu Mar 14 07:01:41 2024 - INFO  - The case exit by code 0.
Thu Mar 14 07:01:41 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Thu Mar 14 07:01:42 2024 - INFO  - start to run testcase:ruyi_test_news.
Thu Mar 14 07:03:16 2024 - INFO  - The case exit by code 0.
Thu Mar 14 07:03:16 2024 - INFO  - End to run testcase:ruyi_test_news.
Thu Mar 14 07:03:16 2024 - INFO  - start to run testcase:ruyi_test_venv.
Thu Mar 14 07:16:02 2024 - INFO  - The case exit by code 0.
Thu Mar 14 07:16:02 2024 - INFO  - End to run testcase:ruyi_test_venv.
Thu Mar 14 07:16:02 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Thu Mar 14 07:17:08 2024 - INFO  - The case exit by code 0.
Thu Mar 14 07:17:08 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Thu Mar 14 07:17:09 2024 - INFO  - start to run testcase:ruyi_test_admin.
Thu Mar 14 07:18:32 2024 - INFO  - The case exit by code 0.
Thu Mar 14 07:18:33 2024 - INFO  - End to run testcase:ruyi_test_admin.
Thu Mar 14 07:18:33 2024 - INFO  - start to run testcase:ruyi_test_device.
Thu Mar 14 07:57:39 2024 - INFO  - The case exit by code 0.
Thu Mar 14 07:57:39 2024 - INFO  - End to run testcase:ruyi_test_device.
Thu Mar 14 07:57:40 2024 - INFO  - A total of 13 use cases were executed, with 13 successes and 0 failures.
```

### 测试结果

共测试了 1 个测试套， 13 个测试用例，其中

+ 13 个测试用例成功

### 测试结论

没有发现问题

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240312/logs/jenkins/56.log)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240312/logs/revyos_riscv64_container/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240312/videos/jenkins/0.6.0_test.mp4)

