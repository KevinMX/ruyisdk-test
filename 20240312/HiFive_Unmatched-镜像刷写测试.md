# RUYI 包管理 0.6.0 版本 HiFive Unmatched 镜像刷写测试结果

本次测试基于 ruyi 0.6.0 版本预编译的 amd64 架构版本二进制 [ruyi.amd64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.5.0/ruyi.amd64)。

由于涉及到硬盘镜像写入和开发板镜像烧录，本次测试手动进行。

### 测试环境说明

+ 测试环境为 Ubuntu 22.04.3 LTS x86_64。

### 测试执行

0. 安装 ruyi CLI: `sudo curl -Lo /usr/bin/ruyi https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.4.0/ruyi.riscv64 && sudo chmod +x /usr/bin/ruyi`
2. 运行 `ruyi device provision`。
3. 按提示操作，分别刷入 Ubuntu 和 openKylin 镜像进行测试。
4. 使用 `eject` 命令弹出读卡器/存储卡，插入开发板，连接串口，检查是否正常启动。

### 测试结果

HiFive Unmatched 的 openKylin 和 Ubuntu 镜像能够被正常写入且正常启动。

### 测试结论

ruyi 0.6.0 amd64 版本的 `device provision` 功能通过测试。

### 测试用例列表

|        测试用例名         |       测试内容       |
|:---------------------:|:----------------:|
| HiFive Unmatched 镜像刷写 | 测试 dd 镜像是否正常 |

### 失败用例列表

| 测试环境 | 测试用例名 | 状态 | 日志文件 | 原因 |
| :-: | :-: | :-: | :-: | :-: |

### 其他信息

屏幕录像如下。

（包括了刷写和开发板启动时的串口输出，为节省仓库空间已 10x 快进处理）

#### Unmatched 启动日志

openKylin:

[![asciicast](https://asciinema.org/a/Wgz7wgCph6BhEQpEskH4LDMd4.svg)](https://asciinema.org/a/Wgz7wgCph6BhEQpEskH4LDMd4)

Ubuntu 23.10:

[![asciicast](https://asciinema.org/a/Rh773h5eOalKZlzjQRFrQDnjY.svg)](https://asciinema.org/a/Rh773h5eOalKZlzjQRFrQDnjY)