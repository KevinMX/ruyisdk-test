# RUYI 包管理 0.6.0 版本 SG2042 openEuler RISC-V 测试结果

本次测试基于 RUYI 0.6.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.5.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 openEuler 23.09 RISC-V preview V1 SG2042 (Milk-V Pioneer v1.3) 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、psmisc、ping、make、python3、python3-paramiko、python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖。
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的。
+ 测试环境为 openEuler 23.09 RISC-V preview V1 on SG2042 (Milk-V Pioneer v1.3)。

### 测试执行

0. 以 `root` 用户身份登录系统。

1. 获取 mugen-ruyi 测试套：`git clone --depth=1 https://github.com/weilinfox/ruyi-mugen`

> 若系统未安装 `git`，需要先行安装：`dnf install -y git`

2. 安装依赖包：`cd ruyi-mugen && bash dep_install.sh`

3. 配置 `mugen`：

`bash mugen.sh -c --ip 127.0.0.1 --password 'openEuler12#$' --user root --port 22`

4. 执行 `ruyi` 测试套：`bash mugen.sh -f ruyi`

5. 执行结束后，对 log 文件名进行处理（避免 Windows 用户无法 checkout 仓库）：

```bash
for file in $(find ./logs -name "*:*"); do mv "$file" "${file//:/_}"; done
```

### 测试结果

共测试了 1 个测试套，13 个测试用例，4 个测试用例成功，9 个测试用例失败。

### 测试结论

`ruyi` 0.6.0 riscv64 版本目前在 openEuler 23.09 RISC-V 上无法执行。

`ruyi` 未能找到系统提供的 `libssl.so` 库，执行失败，详细情况请见下方日志。


### 测试用例列表

| 测试套/软件包名 |             测试用例名             |                 测试内容                  |
|:---------------:|:----------------------------------:|:-------------------------------------:|
|      ruyi       |         ruyi\_test\_common         |               基本命令测试                |
|                 |          ruyi\_test\_xdg           |        ``XDG_*_HOME`` 环境变量测试        |
|                 |          ruyi\_test\_venv          |             ``venv`` 命令测试             |
|                 |         ruyi\_test\_admin          |            ``admin`` 命令测试             |
|                 |      ruyi\_test\_cmake\_ninja      | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|                 |          ruyi\_test\_qemu          |               QEMU 支持测试               |
|                 |      ruyi\_test\_xthead\_qemu      |           平头哥 QEMU 支持测试            |
|                 |          ruyi\_test\_llvm          |               LLVM 支持测试               |
|                 |          ruyi\_test\_news          |             ``news`` 命令测试             |
|                 |         ruyi\_test\_device         |            ``device`` 命令测试            |
|                 | ruyi\_test\_gnu-plct-rv64ilp32-elf |   ``gnu-plct-rv64ilp32-elf`` 工具链测试   |
|                 |         ruyi\_test\_config         |         config.toml 配置文件测试          |
|                 |        ruyi\_test\_binaries        |               二进制包测试                |

### 成功用例列表

| 测试套/软件包名 |             测试用例名             |               测试内容                |
|:---------------:|:----------------------------------:|:---------------------------------:|
|      ruyi       |          ruyi\_test\_qemu          |             QEMU 支持测试             |
|                 |      ruyi\_test\_xthead\_qemu      |         平头哥 QEMU 支持测试          |
|                 |          ruyi\_test\_llvm          |             LLVM 支持测试             |
|                 | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |


### 失败用例列表

| 测试套/软件包名 |        测试用例名        |                 测试内容                  |
|:---------------:|:------------------------:|:-------------------------------------:|
|      ruyi       |    ruyi\_test\_common    |               基本命令测试                |
|                 |     ruyi\_test\_xdg      |        ``XDG_*_HOME`` 环境变量测试        |
|                 |     ruyi\_test\_venv     |             ``venv`` 命令测试             |
|                 |    ruyi\_test\_admin     |            ``admin`` 命令测试             |
|                 | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|                 |     ruyi\_test\_news     |             ``news`` 命令测试             |
|                 |    ruyi\_test\_device    |            ``device`` 命令测试            |
|                 |    ruyi\_test\_config    |         config.toml 配置文件测试          |
|                 |   ruyi\_test\_binaries   |               二进制包测试                |

### 其他信息

详细日志见 [oE2309-riscv64-sg2042](/20240224/logs/oE2309-riscv64-sg2042/) 目录。

此失败原因已提交至 issue[#89](https://github.com/ruyisdk/ruyi/issues/89)

```log
[root@openeuler-riscv64 ruyi-mugen]# bash mugen.sh -f ruyi                                                         
Fri Mar 15 15:34:51 2024 - INFO  - start to run testcase:ruyi_test_device.                                                                                                                                                             
Fri Mar 15 15:35:06 2024 - ERROR - The case exit by code 2.                                                        
Fri Mar 15 15:35:06 2024 - INFO  - End to run testcase:ruyi_test_device.                                                                                                                                                               
Fri Mar 15 15:35:06 2024 - INFO  - start to run testcase:ruyi_test_news.                                           
Fri Mar 15 15:35:25 2024 - ERROR - The case exit by code 4.                                                                                                                                                                            
Fri Mar 15 15:35:25 2024 - INFO  - End to run testcase:ruyi_test_news.                                             
Fri Mar 15 15:35:26 2024 - INFO  - start to run testcase:ruyi_test_common.                                                                                                                                                             
Fri Mar 15 15:36:19 2024 - ERROR - The case exit by code 17.                                                       
Fri Mar 15 15:36:20 2024 - INFO  - End to run testcase:ruyi_test_common.                                           
Fri Mar 15 15:36:20 2024 - INFO  - start to run testcase:ruyi_test_llvm.                                           
Fri Mar 15 15:36:32 2024 - INFO  - The case exit by code 0.                                                        
Fri Mar 15 15:36:32 2024 - INFO  - End to run testcase:ruyi_test_llvm.                                             
Fri Mar 15 15:36:33 2024 - INFO  - start to run testcase:ruyi_test_qemu.                                           
Fri Mar 15 15:36:45 2024 - INFO  - The case exit by code 0.                                                        
Fri Mar 15 15:36:45 2024 - INFO  - End to run testcase:ruyi_test_qemu.                                             
Fri Mar 15 15:36:46 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.                         
Fri Mar 15 15:37:00 2024 - INFO  - The case exit by code 0.                                                        
Fri Mar 15 15:37:00 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.                           
Fri Mar 15 15:37:00 2024 - INFO  - start to run testcase:ruyi_test_xdg.                                            
Fri Mar 15 15:37:30 2024 - ERROR - The case exit by code 11.                                                       
Fri Mar 15 15:37:30 2024 - INFO  - End to run testcase:ruyi_test_xdg.                                              
Fri Mar 15 15:37:31 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.                                    
Fri Mar 15 15:38:26 2024 - ERROR - The case exit by code 2.                                                        
Fri Mar 15 15:38:26 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.                                      
Fri Mar 15 15:38:26 2024 - INFO  - start to run testcase:ruyi_test_binaries.                                       
Fri Mar 15 15:38:41 2024 - ERROR - The case exit by code 1.                                                        
Fri Mar 15 15:38:41 2024 - INFO  - End to run testcase:ruyi_test_binaries.                                         
Fri Mar 15 15:38:41 2024 - INFO  - start to run testcase:ruyi_test_venv.                                           
Fri Mar 15 15:38:57 2024 - ERROR - The case exit by code 3.                                                        
Fri Mar 15 15:38:57 2024 - INFO  - End to run testcase:ruyi_test_venv.                                             
Fri Mar 15 15:38:57 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.                                    
Fri Mar 15 15:39:10 2024 - INFO  - The case exit by code 0.                                                        
Fri Mar 15 15:39:10 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.                                      
Fri Mar 15 15:39:10 2024 - INFO  - start to run testcase:ruyi_test_config.                                         
Fri Mar 15 15:39:25 2024 - ERROR - The case exit by code 4.                                                        
Fri Mar 15 15:39:26 2024 - INFO  - End to run testcase:ruyi_test_config.                                           
Fri Mar 15 15:39:26 2024 - INFO  - start to run testcase:ruyi_test_admin.                                          
Fri Mar 15 15:39:37 2024 - ERROR - The case exit by code 1.                                                        
Fri Mar 15 15:39:37 2024 - INFO  - End to run testcase:ruyi_test_admin.                                            
Fri Mar 15 15:39:37 2024 - INFO  - A total of 13 use cases were executed, with 4 successes and 9 failures.                                                                                                                             
[root@openeuler-riscv64 ruyi-mugen]#
```

#### ruyi 错误日志

```log
[openeuler@openeuler-riscv64 ~]$ RUYI_DEBUG=x ruyi
debug: [2024-03-15T16:04:52.038154] argv[0] = ruyi, self_exe = /home/openeuler/.local/bin/ruyi                                                                                                                                log.py:37debug: [2024-03-15T16:04:52.193057] soname libssl.so not working: libssl.so: cannot open shared object file: No such file or directory                                                                                        log.py:37
Traceback (most recent call last):
  File "/home/openeuler/.cache/ruyi/progcache/0.6.0/__main__.py", line 35, in <module>
  File "/home/openeuler/.cache/ruyi/progcache/0.6.0/ruyi/cli/__init__.py", line 283, in main
  File "/home/openeuler/.cache/ruyi/progcache/0.6.0/ruyi/cli/__init__.py", line 26, in init_argparse
  File "<frozen importlib._bootstrap>", line 1176, in _find_and_load
  File "<frozen importlib._bootstrap>", line 1147, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 690, in _load_unlocked
  File "/home/openeuler/.cache/ruyi/progcache/0.6.0/ruyi/device/provision_cli.py", line 12, in <module ruyi.device.provision_cli>
  File "<frozen importlib._bootstrap>", line 1176, in _find_and_load
  File "<frozen importlib._bootstrap>", line 1147, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 690, in _load_unlocked
  File "/home/openeuler/.cache/ruyi/progcache/0.6.0/ruyi/ruyipkg/atom.py", line 6, in <module ruyi.ruyipkg.atom>
  File "<frozen importlib._bootstrap>", line 1176, in _find_and_load
  File "<frozen importlib._bootstrap>", line 1147, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 690, in _load_unlocked
  File "/home/openeuler/.cache/ruyi/progcache/0.6.0/ruyi/ruyipkg/repo.py", line 6, in <module ruyi.ruyipkg.repo>
  File "<frozen importlib._bootstrap>", line 1176, in _find_and_load
  File "<frozen importlib._bootstrap>", line 1147, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 690, in _load_unlocked
  File "/home/openeuler/.cache/ruyi/progcache/0.6.0/pygit2/__init__.py", line 217, in <module pygit2>
  File "/home/openeuler/.cache/ruyi/progcache/0.6.0/pygit2/settings.py", line 57, in __init__
  File "/home/openeuler/.cache/ruyi/progcache/0.6.0/pygit2/settings.py", line 63, in _initialize_tls_certificate_locations
  File "/home/openeuler/.cache/ruyi/progcache/0.6.0/pygit2/settings.py", line 192, in set_ssl_cert_locations
_pygit2.GitError: OpenSSL error: failed to load certificates: error:00000000:lib(0)::reason(0)
[openeuler@openeuler-riscv64 ~]$ 
```