# RUYI 包管理 QEMU openEuler 23.09 riscv64 测试结果

本次测试基于 RUYI 0.6.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.6.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 QEMU openEuler 23.09 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ openEuler 2309 riscv64 镜像使用软件所提供的镜像 [openEuler 2309 riscv64 base](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/20231130/v0.2/QEMU/openEuler-23.09-V1-base-qemu-testing.qcow2.zst)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Thu Mar 14 13:28:59 2024 - INFO  - start to run testcase:ruyi_test_device.
Thu Mar 14 13:30:41 2024 - ERROR - The case exit by code 2.
Thu Mar 14 13:30:43 2024 - INFO  - End to run testcase:ruyi_test_device.
Thu Mar 14 13:30:48 2024 - INFO  - start to run testcase:ruyi_test_venv.
Thu Mar 14 13:33:04 2024 - ERROR - The case exit by code 3.
Thu Mar 14 13:33:06 2024 - INFO  - End to run testcase:ruyi_test_venv.
Thu Mar 14 13:33:10 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Thu Mar 14 13:34:43 2024 - INFO  - The case exit by code 0.
Thu Mar 14 13:34:45 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Thu Mar 14 13:34:49 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Thu Mar 14 13:36:17 2024 - INFO  - The case exit by code 0.
Thu Mar 14 13:36:19 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Thu Mar 14 13:36:23 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Thu Mar 14 13:38:39 2024 - ERROR - The case exit by code 1.
Thu Mar 14 13:38:41 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Thu Mar 14 13:38:45 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Thu Mar 14 13:42:50 2024 - ERROR - The case exit by code 11.
Thu Mar 14 13:42:52 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Thu Mar 14 13:42:57 2024 - INFO  - start to run testcase:ruyi_test_news.
Thu Mar 14 13:46:02 2024 - ERROR - The case exit by code 4.
Thu Mar 14 13:46:04 2024 - INFO  - End to run testcase:ruyi_test_news.
Thu Mar 14 13:46:08 2024 - INFO  - start to run testcase:ruyi_test_admin.
Thu Mar 14 13:48:12 2024 - ERROR - The case exit by code 1.
Thu Mar 14 13:48:14 2024 - INFO  - End to run testcase:ruyi_test_admin.
Thu Mar 14 13:48:19 2024 - INFO  - start to run testcase:ruyi_test_config.
Thu Mar 14 13:50:55 2024 - ERROR - The case exit by code 4.
Thu Mar 14 13:50:57 2024 - INFO  - End to run testcase:ruyi_test_config.
Thu Mar 14 13:51:03 2024 - INFO  - start to run testcase:ruyi_test_common.
Thu Mar 14 13:59:49 2024 - ERROR - The case exit by code 17.
Thu Mar 14 13:59:51 2024 - INFO  - End to run testcase:ruyi_test_common.
Thu Mar 14 13:59:55 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Thu Mar 14 14:03:04 2024 - INFO  - The case exit by code 0.
Thu Mar 14 14:03:06 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Thu Mar 14 14:03:10 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Thu Mar 14 14:11:46 2024 - ERROR - The case exit by code 2.
Thu Mar 14 14:11:48 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Thu Mar 14 14:11:52 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu Mar 14 14:14:49 2024 - INFO  - The case exit by code 0.
Thu Mar 14 14:14:51 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu Mar 14 14:14:53 2024 - INFO  - A total of 13 use cases were executed, with 4 successes and 9 failures.
```

### 测试结果

共测试了 1 个测试套， 13 个测试用例，其中

+ 4 个测试用例成功
+ 9 个测试用例失败

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 失败日志分析

所有失败原因相同，均为 ruyi 本身运行失败。分析 ruyi\_test\_common 的日志：

```
+ RUYI_DEBUG=x
+ ruyi --version
debug: [2024-03-14T13:53:21.802014] argv[0] = ruyi, self_exe =         log.py:37
                                    /home/openeuler/workspace/workspac          
                                    e/ruyi-mugen_ruyisdk/testcases/cli          
                                    -test/ruyi/ruyi                             
debug: [2024-03-14T13:53:23.399420] soname libssl.so not working:      log.py:37
                                    libssl.so: cannot open shared               
                                    object file: No such file or                
                                    directory                                   
Traceback (most recent call last):
  File "/root/.cache/ruyi/progcache/0.6.0/__main__.py", line 35, in <module>
  File "/root/.cache/ruyi/progcache/0.6.0/ruyi/cli/__init__.py", line 283, in main
  File "/root/.cache/ruyi/progcache/0.6.0/ruyi/cli/__init__.py", line 26, in init_argparse
  File "<frozen importlib._bootstrap>", line 1176, in _find_and_load
  File "<frozen importlib._bootstrap>", line 1147, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 690, in _load_unlocked
  File "/root/.cache/ruyi/progcache/0.6.0/ruyi/device/provision_cli.py", line 12, in <module ruyi.device.provision_cli>
  File "<frozen importlib._bootstrap>", line 1176, in _find_and_load
  File "<frozen importlib._bootstrap>", line 1147, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 690, in _load_unlocked
  File "/root/.cache/ruyi/progcache/0.6.0/ruyi/ruyipkg/atom.py", line 6, in <module ruyi.ruyipkg.atom>
  File "<frozen importlib._bootstrap>", line 1176, in _find_and_load
  File "<frozen importlib._bootstrap>", line 1147, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 690, in _load_unlocked
  File "/root/.cache/ruyi/progcache/0.6.0/ruyi/ruyipkg/repo.py", line 6, in <module ruyi.ruyipkg.repo>
  File "<frozen importlib._bootstrap>", line 1176, in _find_and_load
  File "<frozen importlib._bootstrap>", line 1147, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 690, in _load_unlocked
  File "/root/.cache/ruyi/progcache/0.6.0/pygit2/__init__.py", line 217, in <module pygit2>
  File "/root/.cache/ruyi/progcache/0.6.0/pygit2/settings.py", line 57, in __init__
  File "/root/.cache/ruyi/progcache/0.6.0/pygit2/settings.py", line 63, in _initialize_tls_certificate_locations
  File "/root/.cache/ruyi/progcache/0.6.0/pygit2/settings.py", line 192, in set_ssl_cert_locations
_pygit2.GitError: OpenSSL error: failed to load certificates: error:00000000:lib(0)::reason(0)
```

可见 PyGit2 载入 libssl 失败。关联 issue [#88](https://github.com/ruyisdk/ruyi/issues/88)

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240312/logs/jenkins/56.log)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240312/logs/oE2309-riscv64-qemu/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240312/videos/jenkins/0.6.0_test.mp4)

