# RUYI 包管理 0.5.0 版本 QEMU openEuler 23.09 riscv64 测试结果

本次测试基于 RUYI 0.5.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.5.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 QEMU openEuler 23.09 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ openEuler 2309 riscv64 镜像使用软件所提供的镜像 [openEuler 2309 riscv64 base](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/20231130/v0.2/QEMU/openEuler-23.09-V1-base-qemu-testing.qcow2.zst)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Wed Feb 28 14:49:32 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Wed Feb 28 14:51:40 2024 - INFO  - The case exit by code 0.
Wed Feb 28 14:51:44 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Wed Feb 28 14:51:49 2024 - INFO  - start to run testcase:ruyi_test_admin.
Wed Feb 28 14:53:03 2024 - INFO  - The case exit by code 0.
Wed Feb 28 14:53:05 2024 - INFO  - End to run testcase:ruyi_test_admin.
Wed Feb 28 14:53:09 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Wed Feb 28 14:54:55 2024 - INFO  - The case exit by code 0.
Wed Feb 28 14:54:57 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Wed Feb 28 14:55:01 2024 - INFO  - start to run testcase:ruyi_test_venv.
Wed Feb 28 15:06:54 2024 - INFO  - The case exit by code 0.
Wed Feb 28 15:06:56 2024 - INFO  - End to run testcase:ruyi_test_venv.
Wed Feb 28 15:07:00 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Feb 28 15:11:09 2024 - INFO  - The case exit by code 0.
Wed Feb 28 15:11:11 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Feb 28 15:11:15 2024 - INFO  - start to run testcase:ruyi_test_news.
Wed Feb 28 15:14:10 2024 - INFO  - The case exit by code 0.
Wed Feb 28 15:14:12 2024 - INFO  - End to run testcase:ruyi_test_news.
Wed Feb 28 15:14:16 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Wed Feb 28 15:28:24 2024 - INFO  - The case exit by code 0.
Wed Feb 28 15:28:26 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Wed Feb 28 15:28:29 2024 - INFO  - start to run testcase:ruyi_test_common.
Wed Feb 28 16:01:09 2024 - INFO  - The case exit by code 0.
Wed Feb 28 16:01:10 2024 - INFO  - End to run testcase:ruyi_test_common.
Wed Feb 28 16:01:14 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Wed Feb 28 16:35:15 2024 - INFO  - The case exit by code 0.
Wed Feb 28 16:35:17 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Wed Feb 28 16:35:21 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Wed Feb 28 17:31:18 2024 - INFO  - The case exit by code 0.
Wed Feb 28 17:31:19 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Wed Feb 28 17:31:23 2024 - INFO  - start to run testcase:ruyi_test_device.
Wed Feb 28 18:34:33 2024 - INFO  - The case exit by code 0.
Wed Feb 28 18:34:36 2024 - INFO  - End to run testcase:ruyi_test_device.
Wed Feb 28 18:34:39 2024 - INFO  - A total of 11 use cases were executed, with 11 successes and 0 failures.
```

### 测试结果

共测试了 1 个测试套， 11 个测试用例，其中

+ 11 个测试用例成功

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240224/logs/jenkins.log)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240224/logs/oE2309-riscv64-qemu/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240224/videos/jenkins/0.5.0_test.mp4)

