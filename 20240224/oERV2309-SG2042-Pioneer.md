# RUYI 包管理 0.5.0 版本 SG2042 openEuler RISC-V 测试结果

本次测试基于 RUYI 0.5.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.5.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 openEuler 23.09 RISC-V preview V1 SG2042 (Milk-V Pioneer v1.3) 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、psmisc、ping、make、python3、python3-paramiko、python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖。
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的。
+ 测试环境为 openEuler 23.09 RISC-V preview V1 on SG2042 (Milk-V Pioneer v1.3)。

### 测试执行

0. 以 `root` 用户身份登录系统。

1. 获取 mugen-ruyi 测试套：`git clone --depth=1 https://github.com/weilinfox/ruyi-mugen`

> 若系统未安装 `git`，需要先行安装：`dnf install -y git`

2. 安装依赖包：`cd ruyi-mugen && bash dep_install.sh`

3. 配置 `mugen`：

`bash mugen.sh -c --ip 127.0.0.1 --password 'openEuler12#$' --user root --port 22`

4. 执行 `ruyi` 测试套：`bash mugen.sh -f ruyi`

5. 执行结束后，对 log 文件名进行处理（避免 Windows 用户无法 checkout 仓库）：

```bash
for file in $(find ./logs -name "*:*"); do mv "$file" "${file//:/_}"; done
```

### 测试结果

共测试了 1 个测试套，11 个测试用例，在  openEuler 23.09 RISC-V preview V1 SG2042 上全部测试成功。

openEuler 23.09 RISC-V preview V1:

```log
[root@openeuler-riscv64 ruyi-mugen]# bash mugen.sh -f ruyi
for file in $(find ./logs -name "*:*"); do mv "$file" "${file//:/_}"; done
Tue Feb 27 21:00:32 2024 - INFO  - start to run testcase:ruyi_test_common.
Tue Feb 27 21:02:39 2024 - INFO  - The case exit by code 0.
Tue Feb 27 21:02:39 2024 - INFO  - End to run testcase:ruyi_test_common.
Tue Feb 27 21:02:39 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Tue Feb 27 21:08:04 2024 - INFO  - The case exit by code 0.
Tue Feb 27 21:08:04 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Tue Feb 27 21:08:05 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Tue Feb 27 21:08:21 2024 - INFO  - The case exit by code 0.
Tue Feb 27 21:08:21 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Tue Feb 27 21:08:21 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Tue Feb 27 21:10:04 2024 - INFO  - The case exit by code 0.
Tue Feb 27 21:10:04 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Tue Feb 27 21:10:04 2024 - INFO  - start to run testcase:ruyi_test_news.
Tue Feb 27 21:10:26 2024 - INFO  - The case exit by code 0.
Tue Feb 27 21:10:26 2024 - INFO  - End to run testcase:ruyi_test_news.
Tue Feb 27 21:10:27 2024 - INFO  - start to run testcase:ruyi_test_venv.
Tue Feb 27 21:11:57 2024 - INFO  - The case exit by code 0.
Tue Feb 27 21:11:57 2024 - INFO  - End to run testcase:ruyi_test_venv.
Tue Feb 27 21:11:58 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Tue Feb 27 21:12:14 2024 - INFO  - The case exit by code 0.
Tue Feb 27 21:12:14 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Tue Feb 27 21:12:14 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Tue Feb 27 21:12:47 2024 - INFO  - The case exit by code 0.
Tue Feb 27 21:12:47 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Tue Feb 27 21:12:47 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Tue Feb 27 21:13:04 2024 - INFO  - The case exit by code 0.
Tue Feb 27 21:13:04 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Tue Feb 27 21:13:04 2024 - INFO  - start to run testcase:ruyi_test_admin.
Tue Feb 27 21:13:15 2024 - INFO  - The case exit by code 0.
Tue Feb 27 21:13:15 2024 - INFO  - End to run testcase:ruyi_test_admin.
Tue Feb 27 21:13:15 2024 - INFO  - start to run testcase:ruyi_test_device.
Tue Feb 27 21:46:39 2024 - INFO  - The case exit by code 0.
Tue Feb 27 21:46:39 2024 - INFO  - End to run testcase:ruyi_test_device.
Tue Feb 27 21:46:39 2024 - INFO  - A total of 11 use cases were executed, with 11 successes and 0 failures.
[root@openeuler-riscv64 ruyi-mugen]#
```

### 测试结论

所有测试用例执行成功，mugen 测试通过。没有发现意外的失败。

### 测试用例列表

| 测试套/软件包名 |             测试用例名             |                 测试内容                  |
|:---------------:|:----------------------------------:|:-------------------------------------:|
|      ruyi       |         ruyi\_test\_common         |               基本命令测试                |
|                 |          ruyi\_test\_xdg           |        ``XDG_*_HOME`` 环境变量测试        |
|                 |          ruyi\_test\_venv          |             ``venv`` 命令测试             |
|                 |         ruyi\_test\_admin          |            ``admin`` 命令测试             |
|                 |      ruyi\_test\_cmake\_ninja      | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|                 |          ruyi\_test\_qemu          |               QEMU 支持测试               |
|                 |      ruyi\_test\_xthead\_qemu      |           平头哥 QEMU 支持测试            |
|                 | ruyi\_test\_gnu-plct-rv64ilp32-elf |      `rv64ilp32` ABI 裸机工具链测试       |
|                 |          ruyi\_test\_llvm          |               LLVM 支持测试               |
|                 |          ruyi\_test\_news          |             ``news`` 命令测试             |
|                 |         ruyi\_test\_device         |            ``device`` 命令测试            |


### 失败用例列表

| 测试环境 | 测试用例名 | 状态 | 日志文件 | 原因 |
| :-: | :-: | :-: | :-: | :-: |

### 其他信息

详细日志见 [oE2309-riscv64-sg2042](/20240224/logs/oE2309-riscv64-sg2042/) 目录。


