# RUYI 包管理 0.5.0 版本 CanMV-K230 镜像刷写测试结果

本次测试基于 RUYI 0.5.0 版本预编译的 amd64 架构版本二进制 [ruyi.amd64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.5.0/ruyi.amd64) 。

使用镜像刷写功能刷写 CanMV-K230 镜像并在 CanMV-K230-V1.1 开发板启动，验证启动性并录制视频。

## 测试流程

1. 安装 v0.5.0 版本的 RUYI 包管理器
2. 准备一张容量大于 2GB 的 Micro SD 卡
3. 将 Micro SD 卡与计算机连接，并查看其在 ``/dev`` 下的设备路径
4. 运行 ``ruyi device provision`` 命令
5. 依次选择 ``Canaan Kendryte K230`` 开发板和 ``Canaan Kendryte K230 (generic variant)`` 版本
6. 分别选择列出的镜像列表中的 ``Canaan Kendryte K230 Official CanMV Debian SDK`` 和 ``Canaan Kendryte K230 Official CanMV Ubuntu SDK`` 进行刷写
7. 将 Micro SD 卡插入 CanMV-K230 的 Micro SD 卡槽中
8. 将串口转 USB 模块的串口端子与 CanMV-K230 的 TXD0 、 RXD0 、 GND 正确连接
9. 将串口转 USB 模块与计算机连接，并查看其在 ``/dev`` 下的设备路径
10. 运行 ``sudo minicom -D`` 命令打开串口
11. 使用 Type-C 线缆连接 CanMV-K230 的 Power 口
12. 上电
13. 登陆用户名与密码均为 ``root``

## 测试日志

见视频录制。

## 测试结果

镜像可以正常启动开发板，串口可以正常登陆

## 视频录制

Debian 镜像刷写

![debian\_dd](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240224/videos/CanMV-K230/debian_dd.mp4)

Debian 镜像启动

![debian\_boot](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240224/videos/CanMV-K230/debian_boot.mp4)

Ubuntu 镜像刷写

![ubuntu\_dd](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240224/videos/CanMV-K230/ubuntu_dd.mp4)

Ubuntu 镜像启动

![ubuntu\_boot](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240224/videos/CanMV-K230/ubuntu_boot.mp4)

