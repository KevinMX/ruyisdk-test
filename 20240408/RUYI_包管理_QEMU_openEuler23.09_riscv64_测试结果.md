# RUYI 包管理 QEMU openEuler 23.09 riscv64 测试结果

本次测试基于 RUYI 0.8.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.8.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 QEMU openEuler 23.09 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ openEuler 2309 riscv64 镜像使用软件所提供的镜像 [openEuler 2309 riscv64 base](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/20231130/v0.2/QEMU/openEuler-23.09-V1-base-qemu-testing.qcow2.zst)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Wed Apr 10 18:00:54 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Wed Apr 10 18:52:04 2024 - INFO  - The case exit by code 0.
Wed Apr 10 18:52:06 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Wed Apr 10 18:52:10 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Wed Apr 10 19:00:53 2024 - INFO  - The case exit by code 0.
Wed Apr 10 19:00:55 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Wed Apr 10 19:00:59 2024 - INFO  - start to run testcase:ruyi_test_news.
Wed Apr 10 19:03:53 2024 - INFO  - The case exit by code 0.
Wed Apr 10 19:03:54 2024 - INFO  - End to run testcase:ruyi_test_news.
Wed Apr 10 19:03:58 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Apr 10 19:07:42 2024 - INFO  - The case exit by code 0.
Wed Apr 10 19:07:44 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Apr 10 19:07:48 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Wed Apr 10 19:09:43 2024 - INFO  - The case exit by code 0.
Wed Apr 10 19:09:44 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Wed Apr 10 19:09:48 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Wed Apr 10 19:11:41 2024 - INFO  - The case exit by code 0.
Wed Apr 10 19:11:42 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Wed Apr 10 19:11:46 2024 - INFO  - start to run testcase:ruyi_test_venv.
Wed Apr 10 19:20:47 2024 - INFO  - The case exit by code 0.
Wed Apr 10 19:20:49 2024 - INFO  - End to run testcase:ruyi_test_venv.
Wed Apr 10 19:20:53 2024 - INFO  - start to run testcase:ruyi_test_device.
Wed Apr 10 19:42:57 2024 - INFO  - The case exit by code 0.
Wed Apr 10 19:42:59 2024 - INFO  - End to run testcase:ruyi_test_device.
Wed Apr 10 19:43:03 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Wed Apr 10 20:09:08 2024 - INFO  - The case exit by code 0.
Wed Apr 10 20:09:09 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Wed Apr 10 20:09:13 2024 - INFO  - start to run testcase:ruyi_test_config.
Wed Apr 10 20:11:37 2024 - INFO  - The case exit by code 0.
Wed Apr 10 20:11:38 2024 - INFO  - End to run testcase:ruyi_test_config.
Wed Apr 10 20:11:42 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Wed Apr 10 20:23:11 2024 - INFO  - The case exit by code 0.
Wed Apr 10 20:23:13 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Wed Apr 10 20:23:17 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Wed Apr 10 20:37:10 2024 - INFO  - The case exit by code 0.
Wed Apr 10 20:37:11 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Wed Apr 10 20:37:15 2024 - INFO  - start to run testcase:ruyi_test_common.
Wed Apr 10 20:51:58 2024 - INFO  - The case exit by code 0.
Wed Apr 10 20:52:00 2024 - INFO  - End to run testcase:ruyi_test_common.
Wed Apr 10 20:52:03 2024 - INFO  - start to run testcase:ruyi_test_admin.
Wed Apr 10 20:54:06 2024 - INFO  - The case exit by code 0.
Wed Apr 10 20:54:07 2024 - INFO  - End to run testcase:ruyi_test_admin.
Wed Apr 10 20:54:09 2024 - INFO  - A total of 14 use cases were executed, with 14 successes and 0 failures.
```

### 测试结果

共测试了 1 个测试套， 14 个测试用例，其中

+ 14 个测试用例成功
+ 0 个测试用例失败
+ 0 个测试用例超时

### 测试结论

没有发现问题

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240408/logs/jenkins/)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240408/logs/oE2309-riscv64-qemu/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240408/logs/jenkins/0.8.0_test.mp4)
