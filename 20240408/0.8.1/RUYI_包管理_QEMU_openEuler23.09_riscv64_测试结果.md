# RUYI 包管理 QEMU openEuler 23.09 riscv64 测试结果

本次测试基于 RUYI 0.8.1 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.8.1/ruyi.riscv64) 。

编写了 mugen 测试用例，在 QEMU openEuler 23.09 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ openEuler 2309 riscv64 镜像使用软件所提供的镜像 [openEuler 2309 riscv64 base](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/20231130/v0.2/QEMU/openEuler-23.09-V1-base-qemu-testing.qcow2.zst)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Wed Apr 10 10:08:49 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Wed Apr 10 11:08:53 2024 - WARN  - The case is still running, wait for another 15m.
Wed Apr 10 11:23:55 2024 - WARN  - The case is still running, wait for another 15m.
Wed Apr 10 11:39:00 2024 - WARN  - The case execution timeout.
Wed Apr 10 11:39:02 2024 - ERROR - The case exit by code 143.
Wed Apr 10 11:39:04 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Wed Apr 10 11:39:08 2024 - INFO  - start to run testcase:ruyi_test_news.
Wed Apr 10 11:42:31 2024 - INFO  - The case exit by code 0.
Wed Apr 10 11:42:35 2024 - INFO  - End to run testcase:ruyi_test_news.
Wed Apr 10 11:42:41 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Wed Apr 10 11:57:58 2024 - INFO  - The case exit by code 0.
Wed Apr 10 11:58:00 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Wed Apr 10 11:58:04 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Wed Apr 10 12:31:26 2024 - INFO  - The case exit by code 0.
Wed Apr 10 12:31:28 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Wed Apr 10 12:31:32 2024 - INFO  - start to run testcase:ruyi_test_venv.
Wed Apr 10 12:41:44 2024 - INFO  - The case exit by code 0.
Wed Apr 10 12:41:46 2024 - INFO  - End to run testcase:ruyi_test_venv.
Wed Apr 10 12:41:50 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Wed Apr 10 12:54:36 2024 - INFO  - The case exit by code 0.
Wed Apr 10 12:54:38 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Wed Apr 10 12:54:42 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Wed Apr 10 12:56:49 2024 - INFO  - The case exit by code 0.
Wed Apr 10 12:56:52 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Wed Apr 10 12:56:56 2024 - INFO  - start to run testcase:ruyi_test_admin.
Wed Apr 10 12:59:04 2024 - INFO  - The case exit by code 0.
Wed Apr 10 12:59:06 2024 - INFO  - End to run testcase:ruyi_test_admin.
Wed Apr 10 12:59:10 2024 - INFO  - start to run testcase:ruyi_test_device.
Wed Apr 10 13:22:04 2024 - INFO  - The case exit by code 0.
Wed Apr 10 13:22:05 2024 - INFO  - End to run testcase:ruyi_test_device.
Wed Apr 10 13:22:09 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Wed Apr 10 13:31:10 2024 - INFO  - The case exit by code 0.
Wed Apr 10 13:31:12 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Wed Apr 10 13:31:16 2024 - INFO  - start to run testcase:ruyi_test_config.
Wed Apr 10 13:33:46 2024 - INFO  - The case exit by code 0.
Wed Apr 10 13:33:48 2024 - INFO  - End to run testcase:ruyi_test_config.
Wed Apr 10 13:33:52 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Wed Apr 10 13:35:59 2024 - INFO  - The case exit by code 0.
Wed Apr 10 13:36:01 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Wed Apr 10 13:36:05 2024 - INFO  - start to run testcase:ruyi_test_common.
Wed Apr 10 13:52:51 2024 - INFO  - The case exit by code 0.
Wed Apr 10 13:52:52 2024 - INFO  - End to run testcase:ruyi_test_common.
Wed Apr 10 13:52:56 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Apr 10 13:56:58 2024 - INFO  - The case exit by code 0.
Wed Apr 10 13:57:00 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Apr 10 13:57:01 2024 - INFO  - A total of 14 use cases were executed, with 13 successes and 1 failures.
```

### 测试结果

共测试了 1 个测试套， 14 个测试用例，其中

+ 13 个测试用例成功
+ 1 个测试用例失败
+ 1 个测试用例超时

### 测试结论

此处添加测试结论

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240408/logs/jenkins/)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240408/0.8.1/logs/oE2309-riscv64-qemu/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240408/logs/jenkins/0.8.0_test.mp4)
