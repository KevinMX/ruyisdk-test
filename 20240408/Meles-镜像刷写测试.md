# RUYI 包管理 20240409 版本镜像刷写测试报告

本次测试基于 RUYI 20240409 版本预编译的 amd64 架构版本二进制 [ruyi.amd64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.8.1/ruyi.amd64)。

由于涉及到硬盘镜像写入和开发板镜像烧录，本次测试手动进行。

### 测试环境说明

+ 测试环境为 Ubuntu 22.04.4 LTS x86_64。

### 测试执行

0. 安装 ruyi CLI: `sudo curl -Lo /usr/bin/ruyi https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.8.1/ruyi.amd64 && sudo chmod +x /usr/bin/ruyi`

1. 安装 `fastboot`。

```bash
sudo apt install -y fastboot
```

4. 运行 `ruyi device provision`。

5. 按提示进行操作。

6. 查看镜像是否正确写入，检查开发板能否启动。

### 测试结果

镜像下载和解压成功。由于暂无开发板，未进行实际刷写测试。

### 测试结论

ruyi 20240409 版本的 device provision 功能通过测试。

### 测试用例列表

|     测试用例名      |            测试内容            |
|:---------------:|:--------------------------:|
| Meles 8GB 镜像刷写  | 测试 fastboot 镜像刷写是否正常 |
| Meles 16GB 镜像刷写 | 测试 fastboot 镜像刷写是否正常 |


### 失败用例列表

|       测试环境        |     测试用例名      | 状态 |     日志文件      |     原因     |
|:---------------------:|:---------------:|:--:|:----------------:|:----------:|


### 其他信息

屏幕录像：

[![asciicast](https://asciinema.org/a/0w5hlLAfR7CUUTCbwqcbPbCVu.svg)](https://asciinema.org/a/0w5hlLAfR7CUUTCbwqcbPbCVu)

issue（已解决）: [Github][issue]

[issue]: https://github.com/ruyisdk/ruyi/issues/128