# RUYI 包管理 QEMU openKylin riscv64 测试结果
本次测试基于 RUYI 0.10.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.10.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 QEMU openKylin 1.0.1 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ openKylin 1.0.1 镜像使用 QEMU 无法启动的 [VisionFive2 镜像](https://mirrors.aliyun.com/openkylin-cdimage/1.0.1/openKylin-1.0.1-visionfive2-riscv64.img.xz)替换 [Generic 内核](https://mirrors.aliyun.com/openkylin-cdimage/1.0/openKylin-1.0-hifive-unmatched-riscv64.img.xz)进行测试。

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Thu May 16 16:09:50 2024 - INFO  - start to run testcase:ruyi_test_common.
Thu May 16 16:45:52 2024 - INFO  - The case exit by code 0.
Thu May 16 16:45:53 2024 - INFO  - End to run testcase:ruyi_test_common.
Thu May 16 16:45:55 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu May 16 17:09:18 2024 - INFO  - The case exit by code 0.
Thu May 16 17:09:19 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu May 16 17:09:21 2024 - INFO  - start to run testcase:ruyi_test_config.
Thu May 16 17:21:12 2024 - INFO  - The case exit by code 0.
Thu May 16 17:21:13 2024 - INFO  - End to run testcase:ruyi_test_config.
Thu May 16 17:21:15 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Thu May 16 17:35:44 2024 - INFO  - The case exit by code 0.
Thu May 16 17:35:45 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Thu May 16 17:35:46 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Thu May 16 18:10:08 2024 - INFO  - The case exit by code 0.
Thu May 16 18:10:09 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Thu May 16 18:10:11 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Thu May 16 18:15:22 2024 - INFO  - The case exit by code 0.
Thu May 16 18:15:23 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Thu May 16 18:15:25 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Thu May 16 18:34:39 2024 - INFO  - The case exit by code 0.
Thu May 16 18:34:40 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Thu May 16 18:34:42 2024 - INFO  - start to run testcase:ruyi_test_news.
Thu May 16 18:39:44 2024 - INFO  - The case exit by code 0.
Thu May 16 18:39:45 2024 - INFO  - End to run testcase:ruyi_test_news.
Thu May 16 18:39:47 2024 - INFO  - start to run testcase:ruyi_test_device.
Thu May 16 19:13:47 2024 - INFO  - The case exit by code 0.
Thu May 16 19:13:48 2024 - INFO  - End to run testcase:ruyi_test_device.
Thu May 16 19:13:50 2024 - INFO  - start to run testcase:ruyi_test_venv.
Thu May 16 19:29:48 2024 - INFO  - The case exit by code 0.
Thu May 16 19:29:49 2024 - INFO  - End to run testcase:ruyi_test_venv.
Thu May 16 19:29:50 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Thu May 16 20:26:38 2024 - INFO  - The case exit by code 0.
Thu May 16 20:26:39 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Thu May 16 20:26:41 2024 - INFO  - start to run testcase:ruyi_test_admin.
Thu May 16 20:30:31 2024 - INFO  - The case exit by code 0.
Thu May 16 20:30:32 2024 - INFO  - End to run testcase:ruyi_test_admin.
Thu May 16 20:30:34 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Thu May 16 20:45:40 2024 - INFO  - The case exit by code 0.
Thu May 16 20:45:41 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Thu May 16 20:45:43 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Thu May 16 21:28:21 2024 - INFO  - The case exit by code 0.
Thu May 16 21:28:22 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Thu May 16 21:28:22 2024 - INFO  - A total of 14 use cases were executed, with 14 successes and 0 failures.
```

### 测试结果

共测试了 1 个测试套， 14 个测试用例，其中

+ 14 个测试用例成功
+ 0 个测试用例失败
+ 0 个测试用例超时

### 测试结论

没有发现问题

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240514/logs/jenkins/)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240514/logs/openkylin_riscv64_qemu/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240514/logs/jenkins/0.10.0_test.mp4)
