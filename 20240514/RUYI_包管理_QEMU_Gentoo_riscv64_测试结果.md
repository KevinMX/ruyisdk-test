# RUYI 包管理 QEMU Gentoo Linux riscv64 测试结果
本次测试基于 RUYI 0.10.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.10.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 QEMU Gentoo Linux riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ Gentoo Linux 镜像为以 Gentoo Linux OpenRC Stage 3 tarball 为基础安装的镜像 [stage3 rv64gc lp64d openrc](https://distfiles.gentoo.org/releases/riscv/autobuilds/20240412T170417Z/stage3-rv64_lp64d-openrc-20240412T170417Z.tar.xz)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Fri May 17 05:44:06 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Fri May 17 05:56:25 2024 - ERROR - The case exit by code 1.
Fri May 17 05:56:26 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Fri May 17 05:56:28 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Fri May 17 05:59:08 2024 - INFO  - The case exit by code 0.
Fri May 17 05:59:09 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Fri May 17 05:59:12 2024 - INFO  - start to run testcase:ruyi_test_device.
Fri May 17 06:32:02 2024 - INFO  - The case exit by code 0.
Fri May 17 06:32:03 2024 - INFO  - End to run testcase:ruyi_test_device.
Fri May 17 06:32:06 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Fri May 17 06:46:35 2024 - INFO  - The case exit by code 0.
Fri May 17 06:46:36 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Fri May 17 06:46:39 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Fri May 17 07:14:00 2024 - INFO  - The case exit by code 0.
Fri May 17 07:14:01 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Fri May 17 07:14:04 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Fri May 17 07:56:19 2024 - INFO  - The case exit by code 0.
Fri May 17 07:56:20 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Fri May 17 07:56:23 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Fri May 17 08:05:41 2024 - INFO  - The case exit by code 0.
Fri May 17 08:05:42 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Fri May 17 08:05:44 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Fri May 17 08:08:17 2024 - INFO  - The case exit by code 0.
Fri May 17 08:08:18 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Fri May 17 08:08:20 2024 - INFO  - start to run testcase:ruyi_test_config.
Fri May 17 08:11:06 2024 - INFO  - The case exit by code 0.
Fri May 17 08:11:07 2024 - INFO  - End to run testcase:ruyi_test_config.
Fri May 17 08:11:09 2024 - INFO  - start to run testcase:ruyi_test_admin.
Fri May 17 08:13:46 2024 - INFO  - The case exit by code 0.
Fri May 17 08:13:47 2024 - INFO  - End to run testcase:ruyi_test_admin.
Fri May 17 08:13:49 2024 - INFO  - start to run testcase:ruyi_test_news.
Fri May 17 08:17:12 2024 - INFO  - The case exit by code 0.
Fri May 17 08:17:13 2024 - INFO  - End to run testcase:ruyi_test_news.
Fri May 17 08:17:16 2024 - INFO  - start to run testcase:ruyi_test_venv.
Fri May 17 08:26:10 2024 - INFO  - The case exit by code 0.
Fri May 17 08:26:11 2024 - INFO  - End to run testcase:ruyi_test_venv.
Fri May 17 08:26:13 2024 - INFO  - start to run testcase:ruyi_test_common.
Fri May 17 08:42:02 2024 - INFO  - The case exit by code 0.
Fri May 17 08:42:03 2024 - INFO  - End to run testcase:ruyi_test_common.
Fri May 17 08:42:05 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Fri May 17 08:46:17 2024 - INFO  - The case exit by code 0.
Fri May 17 08:46:18 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Fri May 17 08:46:19 2024 - INFO  - A total of 14 use cases were executed, with 13 successes and 1 failures.
```

### 测试结果

共测试了 1 个测试套， 14 个测试用例，其中

+ 13 个测试用例成功
+ 1 个测试用例失败
+ 0 个测试用例超时

### 测试结论

此处添加测试结论

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240514/logs/jenkins/)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240514/logs/gentoo_riscv64_qemu/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240514/logs/jenkins/0.10.0_test.mp4)
