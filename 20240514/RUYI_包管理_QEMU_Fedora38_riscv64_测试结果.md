# RUYI 包管理 QEMU Fedora38 riscv64 测试结果
本次测试基于 RUYI 0.10.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.10.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 QEMU Fedora38 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ Fedora38 镜像使用 koji 上的 20230825 开发者每日构建镜像 [Fedora-Developer-38-20230825](http://fedora.riscv.rocks/kojifiles/work/tasks/5889/1465889/Fedora-Developer-38-20230825.n.0-sda.raw.xz)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Fri May 17 09:38:23 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Fri May 17 09:38:45 2024 - INFO  - The case exit by code 0.
Fri May 17 09:38:45 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Fri May 17 09:38:46 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Fri May 17 09:41:15 2024 - INFO  - The case exit by code 0.
Fri May 17 09:41:15 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Fri May 17 09:41:16 2024 - INFO  - start to run testcase:ruyi_test_config.
Fri May 17 09:41:47 2024 - INFO  - The case exit by code 0.
Fri May 17 09:41:47 2024 - INFO  - End to run testcase:ruyi_test_config.
Fri May 17 09:41:48 2024 - INFO  - start to run testcase:ruyi_test_common.
Fri May 17 09:44:50 2024 - INFO  - The case exit by code 0.
Fri May 17 09:44:50 2024 - INFO  - End to run testcase:ruyi_test_common.
Fri May 17 09:44:50 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Fri May 17 09:45:12 2024 - INFO  - The case exit by code 0.
Fri May 17 09:45:12 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Fri May 17 09:45:12 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Fri May 17 09:51:03 2024 - INFO  - The case exit by code 0.
Fri May 17 09:51:03 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Fri May 17 09:51:04 2024 - INFO  - start to run testcase:ruyi_test_news.
Fri May 17 09:51:39 2024 - INFO  - The case exit by code 0.
Fri May 17 09:51:39 2024 - INFO  - End to run testcase:ruyi_test_news.
Fri May 17 09:51:39 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Fri May 17 09:53:59 2024 - INFO  - The case exit by code 0.
Fri May 17 09:53:59 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Fri May 17 09:53:59 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Fri May 17 10:01:16 2024 - INFO  - The case exit by code 0.
Fri May 17 10:01:16 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Fri May 17 10:01:16 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Fri May 17 10:01:40 2024 - INFO  - The case exit by code 0.
Fri May 17 10:01:41 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Fri May 17 10:01:41 2024 - INFO  - start to run testcase:ruyi_test_venv.
Fri May 17 10:03:35 2024 - INFO  - The case exit by code 0.
Fri May 17 10:03:36 2024 - INFO  - End to run testcase:ruyi_test_venv.
Fri May 17 10:03:36 2024 - INFO  - start to run testcase:ruyi_test_admin.
Fri May 17 10:03:53 2024 - INFO  - The case exit by code 0.
Fri May 17 10:03:53 2024 - INFO  - End to run testcase:ruyi_test_admin.
Fri May 17 10:03:53 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Fri May 17 10:06:25 2024 - INFO  - The case exit by code 0.
Fri May 17 10:06:26 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Fri May 17 10:06:26 2024 - INFO  - start to run testcase:ruyi_test_device.
Fri May 17 10:22:53 2024 - INFO  - The case exit by code 0.
Fri May 17 10:22:53 2024 - INFO  - End to run testcase:ruyi_test_device.
Fri May 17 10:22:53 2024 - INFO  - A total of 14 use cases were executed, with 14 successes and 0 failures.
```

### 测试结果

共测试了 1 个测试套， 14 个测试用例，其中

+ 14 个测试用例成功
+ 0 个测试用例失败
+ 0 个测试用例超时

### 测试结论

没有发现问题

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240514/logs/jenkins/)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240514/logs/fedora38-riscv64-qemu/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240514/logs/jenkins/0.10.0_test.mp4)
