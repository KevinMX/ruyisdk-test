# RUYI 包管理 Container Debian sid riscv64 测试结果

本次测试基于 RUYI 0.10.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.10.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 systemd-nspawn Debian sid riscv64 环境开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ Debian sid riscv64 镜像为使用 ``mmdebstrap`` 制作的容器

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Thu May 16 10:57:35 2024 - INFO  - start to run testcase:ruyi_test_admin.
Thu May 16 11:03:10 2024 - INFO  - The case exit by code 0.
Thu May 16 11:03:11 2024 - INFO  - End to run testcase:ruyi_test_admin.
Thu May 16 11:03:13 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Thu May 16 11:16:25 2024 - INFO  - The case exit by code 0.
Thu May 16 11:16:26 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Thu May 16 11:16:27 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Thu May 16 11:20:45 2024 - INFO  - The case exit by code 0.
Thu May 16 11:20:46 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Thu May 16 11:20:48 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu May 16 11:27:32 2024 - INFO  - The case exit by code 0.
Thu May 16 11:27:33 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu May 16 11:27:35 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Thu May 16 12:16:28 2024 - INFO  - The case exit by code 0.
Thu May 16 12:16:28 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Thu May 16 12:16:31 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Thu May 16 12:26:06 2024 - INFO  - The case exit by code 0.
Thu May 16 12:26:07 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Thu May 16 12:26:09 2024 - INFO  - start to run testcase:ruyi_test_config.
Thu May 16 12:33:53 2024 - INFO  - The case exit by code 0.
Thu May 16 12:33:54 2024 - INFO  - End to run testcase:ruyi_test_config.
Thu May 16 12:33:56 2024 - INFO  - start to run testcase:ruyi_test_news.
Thu May 16 12:39:10 2024 - INFO  - The case exit by code 0.
Thu May 16 12:39:11 2024 - INFO  - End to run testcase:ruyi_test_news.
Thu May 16 12:39:13 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Thu May 16 13:06:42 2024 - INFO  - The case exit by code 0.
Thu May 16 13:06:43 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Thu May 16 13:06:45 2024 - INFO  - start to run testcase:ruyi_test_device.
Thu May 16 13:42:43 2024 - INFO  - The case exit by code 0.
Thu May 16 13:42:44 2024 - INFO  - End to run testcase:ruyi_test_device.
Thu May 16 13:42:46 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Thu May 16 13:59:17 2024 - INFO  - The case exit by code 0.
Thu May 16 13:59:18 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Thu May 16 13:59:20 2024 - INFO  - start to run testcase:ruyi_test_common.
Thu May 16 14:15:40 2024 - INFO  - The case exit by code 0.
Thu May 16 14:15:41 2024 - INFO  - End to run testcase:ruyi_test_common.
Thu May 16 14:15:43 2024 - INFO  - start to run testcase:ruyi_test_venv.
Thu May 16 14:27:47 2024 - INFO  - The case exit by code 0.
Thu May 16 14:27:48 2024 - INFO  - End to run testcase:ruyi_test_venv.
Thu May 16 14:27:50 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Thu May 16 14:43:11 2024 - INFO  - The case exit by code 0.
Thu May 16 14:43:12 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Thu May 16 14:43:13 2024 - INFO  - A total of 14 use cases were executed, with 14 successes and 0 failures.
```

### 测试结果

共测试了 1 个测试套， 14 个测试用例，其中

+ 14 个测试用例成功
+ 0 个测试用例失败
+ 0 个测试用例超时

### 测试结论

没有发现问题

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240514/logs/jenkins/)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240514/logs/debiansid_riscv64_container/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240514/logs/jenkins/0.10.0_test.mp4)
