# RUYI 包管理 device provision 测试结果

本次测试基于 RUYI 0.10.0 版本预编译的 amd64 架构版本二进制 [ruyi.amd64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.10.0/ruyi.amd64) 。

## 测试流程

手动测试。

```
$ wget https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.10.0/ruyi.amd64
$ chmod +x ./ruyi.amd64
$ sudo cp ./ruyi.amd64 /usr/local/bin/ruyi
```

由于本次新增均为文档，故只验证文档可用性。

## 测试结论

暂没有发现文档链接失败的问题。

