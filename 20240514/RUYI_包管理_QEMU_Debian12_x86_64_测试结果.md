# RUYI 包管理 QEMU Debian12 x86\_64 测试结果
本次测试基于 RUYI 0.10.0 版本预编译的 amd64 架构版本二进制 [ruyi.amd64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.10.0/ruyi.amd64) 。

编写了 mugen 测试用例，在 QEMU Debian12 x86\_64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ Debian12 镜像使用 libguestfs 提供的镜像 [Debian12](https://builder.libguestfs.org/debian-12.xz)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Thu May 16 07:55:15 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Thu May 16 08:55:59 2024 - WARN  - The case is still running, wait for another 15m.
Thu May 16 09:10:59 2024 - WARN  - The case is still running, wait for another 15m.
Thu May 16 09:25:59 2024 - WARN  - The case execution timeout.
Thu May 16 09:26:00 2024 - ERROR - The case exit by code 143.
Thu May 16 09:26:00 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Thu May 16 09:26:00 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Thu May 16 09:46:48 2024 - INFO  - The case exit by code 0.
Thu May 16 09:46:48 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Thu May 16 09:46:49 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Thu May 16 10:05:04 2024 - INFO  - The case exit by code 0.
Thu May 16 10:05:04 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Thu May 16 10:05:05 2024 - INFO  - start to run testcase:ruyi_test_news.
Thu May 16 10:05:31 2024 - INFO  - The case exit by code 0.
Thu May 16 10:05:31 2024 - INFO  - End to run testcase:ruyi_test_news.
Thu May 16 10:05:31 2024 - INFO  - start to run testcase:ruyi_test_common.
Thu May 16 10:08:14 2024 - INFO  - The case exit by code 0.
Thu May 16 10:08:14 2024 - INFO  - End to run testcase:ruyi_test_common.
Thu May 16 10:08:14 2024 - INFO  - start to run testcase:ruyi_test_config.
Thu May 16 10:08:47 2024 - INFO  - The case exit by code 0.
Thu May 16 10:08:47 2024 - INFO  - End to run testcase:ruyi_test_config.
Thu May 16 10:08:47 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Thu May 16 10:29:01 2024 - INFO  - The case exit by code 0.
Thu May 16 10:29:01 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Thu May 16 10:29:01 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Thu May 16 10:48:54 2024 - INFO  - The case exit by code 0.
Thu May 16 10:48:54 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Thu May 16 10:48:54 2024 - INFO  - start to run testcase:ruyi_test_admin.
Thu May 16 10:51:39 2024 - INFO  - The case exit by code 0.
Thu May 16 10:51:39 2024 - INFO  - End to run testcase:ruyi_test_admin.
Thu May 16 10:51:39 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Thu May 16 10:56:49 2024 - INFO  - The case exit by code 0.
Thu May 16 10:56:49 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Thu May 16 10:56:49 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Thu May 16 11:23:13 2024 - INFO  - The case exit by code 0.
Thu May 16 11:23:13 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Thu May 16 11:23:13 2024 - INFO  - start to run testcase:ruyi_test_venv.
Thu May 16 11:29:52 2024 - INFO  - The case exit by code 0.
Thu May 16 11:29:52 2024 - INFO  - End to run testcase:ruyi_test_venv.
Thu May 16 11:29:53 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu May 16 11:32:11 2024 - INFO  - The case exit by code 0.
Thu May 16 11:32:11 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu May 16 11:32:11 2024 - INFO  - start to run testcase:ruyi_test_device.
Thu May 16 11:54:22 2024 - INFO  - The case exit by code 0.
Thu May 16 11:54:22 2024 - INFO  - End to run testcase:ruyi_test_device.
Thu May 16 11:54:22 2024 - INFO  - A total of 14 use cases were executed, with 13 successes and 1 failures.
```

### 测试结果

共测试了 1 个测试套， 14 个测试用例，其中

+ 13 个测试用例成功
+ 1 个测试用例失败
+ 1 个测试用例超时

### 测试结论

此处添加测试结论

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240514/logs/jenkins/)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240514/logs/debian12-x86_64-qemu/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240514/logs/jenkins/0.10.0_test.mp4)
