# RuyiSDK v0.10 版本测试报告

修订记录

| 日期        | 修订版本  | 修改  章节 | 修改描述 | 作者              |
| --------- | ----- | ------ | ---- | --------------- |
| 2024-05-21 | 1.0.0 |        |   | PLCT Lab 第三测试小队 |

目 录

1. 概述
2. 测试策略
3. 测试方法
4. 测试报告
5. 遗留缺陷

## 1. 概述

如意SDK旨在为 RISC-V 开发者提供一个一体化集成开发环境。为普通 RISC-V 用户提供相同的开发流程，让用户可以在不同的开源工具和制造商定制的工具链之间轻松切换。对于用户购买到的任何一款如意 SDK支持的 RISC-V 开发板或模组，都可通过如意 SDK 系统获得硬件资料说明、固件和软件更新、系统性开发环境支持、软硬件调试支持。开发者可以轻松获取任何常用的 RISC-V 扩展指令集架构组合的系统软件支持，通过如意 SDK 系统便捷地生成客户所需的操作系统、工具链、语言执行环境(运行时或虚拟机)、计算库和应用框架。如意SDK支持和持续维护 RISC-V Vector 0.7.1和 RVP 0.5.2 等已经大规模硅化的草案标准和一些厂商定制扩展。

如意 SDK V1.0主要聚焦 RuyiSDK 基础框架的实现与主体环节的打通，发布内容主要包含6个方面。

1. Toolchain：GNU 工具链对 RISC-V 扩展指令集的多版本支持和系统适配

2. Sysroot：openEuler，RevyOS 在内的2种操作系统的交叉环境和本地环境支持

3. 模拟器：整合了 Xuantie CPUs 的扩展实现，并模拟了 LicheeRV 和 LicheePi4A 的硬件环境

4. 硬件镜像：提供 Lichee RV，Lichee Pi 4A ，Milk-V Pioneer， K230 等多平台配套镜像

5. 辅助工具：提供了自动化构建与便捷交互使用工具

6. RuyiSDK 文档：介绍了工具链，模拟器，镜像和辅助工具的构建与使用方法

详细参考： https://github.com/ruyisdk/ 和 https://github.com/ruyisdk/ruyi

根据RuyiSDK发展路线，2023年12月推出首个版本v0.2 (已通过测试按期发布)，从2024年1月16日起按每两个工作周为周期，发布新开发功能，并在发布一周后给出测试报告。

- 2024.01.16 RuyiSDK v0.3

- 2024.01.30 RuyiSDK V0.4

- 2024.02.24 RuyiSDK V0.5

- 2024.03.12 RuyiSDK V0.6

- 2024.03.26 RuyiSDK V0.7 （包括v0.71）

- 2024.04.08 RuyiSDK V0.8 （包括v0.81）

- 2024.04.23 RuyiSDK V0.9 

- 2024.05.14 RuyiSDK V0.10

## 2. 测试策略

RuyiSDK 版本测试按照RuyiSDK团队制定的版本发布计划规划相应的测试活动，测试以用户的视角，基于[RuyiSDK 版本用户手册（同步增加）](https://ruyisdk.github.io/docs/zh/introduction/)，测试检验每个功能是否能正常使用。本测试只验证Ruyi工具的使用，不涉及使用GCC、LLVM等具体工具链的使用。

### 2.1 测试计划

每两周敏捷开发版本的测试为开发中的测试，测试任务包括：1. 开发新功能自动化测试用例，并提交到组内仓库；2. RuyiSDK可自动化测试部分进行全量测试，包括测试新加的功能，跳过暂未实现自动化测试的历史继承特性测试；3. 回归测试遗留缺陷； 4. 缺陷提交issue到项目repo； 5.在使用手册上添加新功能的使用方法。 

表1. RuyiSDK 2周敏捷版本测试计划（每2周收到一个新的版本和简要功能描述）

| 测试阶段   | 起始时间      | 结束时间                   | Days | 备注     |
| ------ | --------- | ---------------------- | ---- | ------ |
| 敏捷测试|      |                   | 7    | 自动化测试用例开发，新功能测试，缺陷回归测试，缺陷ISSUE提交，测试报告，使用文档添加（新功能） |

* 当2周发布功能点少，不影响敏捷测试进度时，可安排专门人员手工快速对新增功能点进行快速验证。

### 2.2 入口标准

1. 上个阶段无 block 问题遗留。
2. 转测版本的冒烟无阻塞性问题。
3. 满足各阶段版本转测检查项。

### 2.3 出口标准

1. 策略规划的测试活动涉及的测试用例已执行完毕。
2. 发布特性满足版本规划目标。
3. 版本无阻塞问题遗留，其它严重问题有相应规避措施或说明。

## 3. 测试方法

测试目的：开发中的测试在RuyiSDK 每2周敏捷开发版本或里程碑正式发布版本开发完成前进行，协助开发人员完成开发过程中的缺陷发现和修复。

测试方法：新加功能的测试，遗留缺陷的回归测试，和已实现自动测试部分的全量测试（用于验证Ruyi工具的功能点），功能点参考[RuyiSDK 版本用户手册](https://ruyisdk.github.io/docs/zh/introduction/)，遗留缺陷参考项目ISSUE。

1. Mugen测试：根据功能点，开发mugen测试套和测试用例，验证实际输出和预期输出的一致性。
2. openQA测试：针对硬件开发板平台和GUI图像化应用使用，根据功能点，开发openQA测试程序，验证开发板的启动、实际命令行和图形化输出和预期输出的一致性。
3. 缺陷回归测试。

正确判定标准：按照[RuyiSDK 版本用户手册(功能跟随版本更新)](https://ruyisdk.github.io/docs/zh/introduction/)，实际输出和预期输出的一致。

## 4. 测试报告

1. [SG2042 Pioneer Fedora 38 测试报告](./RUYI_包管理_Pioneer_Box_Fedora38_riscv64_测试结果.md)
2. [SG2042 Pioneer openEuler 23.09 测试报告](./RUYI_包管理_Pioneer_Box_openEuler23.09_riscv64_测试结果.md)
3. [LPi4A openEuler 23.09 测试报告](./RUYI_包管理_LicheePi4A_openEuler23.09_riscv64_测试结果.md)
4. [LPi4A RevyOS openEuler 23.09 测试报告](./RUYI_包管理_LicheePi4A_RevyOS_riscv64_测试结果.md)
5. [Container RevyOS riscv64 测试结果](./RUYI_包管理_Container_RevyOS_riscv64_测试结果.md)
6. [Container Archlinux riscv64 测试结果](./RUYI_包管理_Container_Archlinux_riscv64_测试结果.md)
7. [Container Archlinux x86_64 测试结果](./RUYI_包管理_Container_Archlinux_x86_64_测试结果.md)
8. [Container Debian sid riscv64 测试结果](./RUYI_包管理_Container_Debiansid_riscv64_测试结果.md)
9. [QEMU Fedora38 x86_64 测试报告](./RUYI_包管理_QEMU_Fedora38_x86_64_测试结果.md)
10. [QEMU Fedora38 riscv64 测试结果](./RUYI_包管理_QEMU_Fedora38_riscv64_测试结果.md)
11. [QEMU openEuler23.09 x86_64 测试报告](./RUYI_包管理_QEMU_openEuler23.09_x86_64_测试结果.md)
12. [QEMU Ubuntu22.04 x86_64 测试报告](./RUYI_包管理_QEMU_Ubuntu22.04_x86_64_测试结果.md)
13. [QEMU Ubuntu 22.04 riscv64 测试结果](./RUYI_包管理_QEMU_Ubuntu22.04_riscv64_测试结果.md)
14. [QEMU openEuler23.09 riscv64 测试报告](./RUYI_包管理_QEMU_openEuler23.09_riscv64_测试结果.md)
15. [QEMU openEuler 23.09 x86_64 测试结果](./RUYI_包管理_QEMU_openEuler23.09_x86_64_测试结果.md)
16. [QEMU Debian12 aarch64 测试结果](./RUYI_包管理_QEMU_Debian12_aarch64_测试结果.md)
17. [QEMU Debian12 x86_64 测试结果](./RUYI_包管理_QEMU_Debian12_x86_64_测试结果.md)
18. [QEMU Gentoo Linux riscv64 测试结果（新增支持）](./RUYI_包管理_QEMU_Gentoo_riscv64_测试结果.md)
19. [QEMU Gentoo Linux x86_64 测试结果（新增支持）](./RUYI_包管理_QEMU_Gentoo_x86_64_测试结果.md)
20. [QEMU openKylin Linux riscv64 测试结果（新增支持）](./RUYI_包管理_QEMU_openKylin_riscv64_测试结果.md)
21. [RUYI 包管理 device provision 测试结果](./RUYI_包管理_device_provision_测试结果.md)

## 5. 缺陷

1. [ruyi device provision dd 相关 sudo 输出格式问题](https://github.com/ruyisdk/ruyi/issues/146)
2. [ruyi list 输出格式发生变化](https://github.com/ruyisdk/ruyi/issues/142)

## 6. 资源

1. [本版本mugen测试程序](https://gitee.com/yunxiangluo/mugen-ruyi/pulls/14)

2. [Ruyi手册](https://ruyisdk.github.io/docs/zh/ruyi/updates/0.10.0.html)
