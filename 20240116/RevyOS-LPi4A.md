# RUYI 包管理 20240116 版本   测试报告

本次测试基于 RUYI 20240116 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64.20240116](https://mirror.iscas.ac.cn/ruyisdk/ruyi/testing/ruyi.riscv64.20240116)。

编写了 mugen 测试用例，在 RevyOS 5.10.113 LPi4A 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、psmisc、ping、make、python3、python3-paramiko、python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖。
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的。
+ 测试环境为RevyOS 5.10.113 LPi4A

### 测试执行

0. 以 `root` 用户身份登录系统。

1. 获取 mugen-ruyi 测试套：`git clone --depth=1 https://github.com/weilinfox/ruyi-mugen`

> 若系统未安装 `git`，需要先行安装：`dnf install -y git`

2. 安装依赖包：`cd mugen-ruyi && bash dep_install.sh`

3. 配置 `mugen`：

openEuler 23.09: `bash mugen.sh -c --ip 127.0.0.1 --password 'openEuler12#$' --user root --port 22`

4. 执行 `ruyi` 测试套：`bash mugen.sh -f ruyi`

5. 执行结束后，对 log 文件名进行处理（避免 Windows 用户无法 checkout 仓库）：

```bash
for file in $(find ./logs -name "*:*"); do mv "$file" "${file//:/_}"; done
```

### 测试结果

共测试了 1 个测试套，9 个测试用例，在  RevyOS 5.10.113 LPi4A 上全部测试成功。

### 测试结论

所有测试用例执行成功，mugen 测试通过。没有发现意外的失败。

### 测试用例列表

| 测试套/软件包名 |        测试用例名        |                  测试内容                   |
| :-------------: | :----------------------: | :-----------------------------------------: |
|      ruyi       |       oe_test_ruyi       |                基本命令测试                 |
|                 |     oe_test_ruyi_xdg     |         ``XDG_*_HOME`` 环境变量测试         |
|                 |    oe_test_ruyi_venv     |              ``venv`` 命令测试              |
|                 |    oe_test_ruyi_admin    |             ``admin`` 命令测试              |
|                 | oe_test_ruyi_cmake_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|                 |    oe_test_ruyi_qemu     |                QEMU 支持测试                |
|                 | oe_test_ruyi_xthead_qemu |            平头哥 QEMU 支持测试             |
|                 |    oe_test_ruyi_llvm     |                LLVM 支持测试                |
|                 |    oe_test_ruyi_news     |               `news`功能测试                |




### 失败用例列表

| 测试环境 | 测试用例名 | 状态 | 日志文件 | 原因 |
| :-: | :-: | :-: | :-: | :-: |

### 其他信息

详细日志见 [oE2309-riscv64-lpi4a](/20240116/logs/RevyOS-riscv64-lpi4a/) 目录。

