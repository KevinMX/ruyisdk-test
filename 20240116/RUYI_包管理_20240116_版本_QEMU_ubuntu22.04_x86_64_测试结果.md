# RUYI 包管理 20240116 版本 QEMU Ubuntu 22.04 x86_64 测试结果

本次测试基于 RUYI 20240116 版本预编译的 amd64 架构版本二进制 [ruyi.amd64.20240116](https://mirror.iscas.ac.cn/ruyisdk/ruyi/testing/ruyi.amd64.20240116) 。

编写了 mugen 测试用例，在 QEMU ubuntu 22.04 x86_64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect 、 psmisc 、 ping 、 make 、 python3 、 python3-paramiko 、 python3-six ，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ Ubuntu 22.04 LTS 镜像使用 Ubuntu 提供的 QEMU 镜像 [ubuntu-22.04-server-cloudimg-amd64](https://cloud-images.ubuntu.com/releases/jammy/release/ubuntu-22.04-server-cloudimg-amd64-disk-kvm.img)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ bash mugen.sh -f ruyi -x
Thu Jan 18 13:53:24 2024 - INFO  - start to run testcase:oe_test_ruyi_cmake_ninja.
Thu Jan 18 14:00:13 2024 - INFO  - The case exit by code 0.
Thu Jan 18 14:00:13 2024 - INFO  - End to run testcase:oe_test_ruyi_cmake_ninja.
Thu Jan 18 14:00:14 2024 - INFO  - start to run testcase:oe_test_ruyi_news.
Thu Jan 18 14:01:02 2024 - INFO  - The case exit by code 0.
Thu Jan 18 14:01:02 2024 - INFO  - End to run testcase:oe_test_ruyi_news.
Thu Jan 18 14:01:02 2024 - INFO  - start to run testcase:oe_test_ruyi_admin.
Thu Jan 18 14:01:48 2024 - INFO  - The case exit by code 0.
Thu Jan 18 14:01:48 2024 - INFO  - End to run testcase:oe_test_ruyi_admin.
Thu Jan 18 14:01:48 2024 - INFO  - start to run testcase:oe_test_ruyi_qemu.
Thu Jan 18 14:04:35 2024 - INFO  - The case exit by code 0.
Thu Jan 18 14:04:35 2024 - INFO  - End to run testcase:oe_test_ruyi_qemu.
Thu Jan 18 14:04:35 2024 - INFO  - start to run testcase:oe_test_ruyi_xdg.
Thu Jan 18 14:06:59 2024 - ERROR - The case exit by code 1.
Thu Jan 18 14:06:59 2024 - INFO  - End to run testcase:oe_test_ruyi_xdg.
Thu Jan 18 14:06:59 2024 - INFO  - start to run testcase:oe_test_ruyi_device.
Thu Jan 18 14:07:58 2024 - INFO  - The case exit by code 0.
Thu Jan 18 14:07:58 2024 - INFO  - End to run testcase:oe_test_ruyi_device.
Thu Jan 18 14:07:58 2024 - INFO  - start to run testcase:oe_test_ruyi_venv.
Thu Jan 18 14:10:23 2024 - INFO  - The case exit by code 0.
Thu Jan 18 14:10:24 2024 - INFO  - End to run testcase:oe_test_ruyi_venv.
Thu Jan 18 14:10:24 2024 - INFO  - start to run testcase:oe_test_ruyi_xthead_qemu.
Thu Jan 18 14:14:17 2024 - INFO  - The case exit by code 0.
Thu Jan 18 14:14:17 2024 - INFO  - End to run testcase:oe_test_ruyi_xthead_qemu.
Thu Jan 18 14:14:18 2024 - INFO  - start to run testcase:oe_test_ruyi_llvm.
Thu Jan 18 14:19:07 2024 - INFO  - The case exit by code 0.
Thu Jan 18 14:19:07 2024 - INFO  - End to run testcase:oe_test_ruyi_llvm.
Thu Jan 18 14:19:07 2024 - INFO  - start to run testcase:oe_test_ruyi.
Thu Jan 18 14:21:51 2024 - ERROR - The case exit by code 1.
Thu Jan 18 14:21:51 2024 - INFO  - End to run testcase:oe_test_ruyi.
Thu Jan 18 14:21:51 2024 - INFO  - A total of 10 use cases were executed, with 8 successes and 2 failures.
```

### 测试结果

共测试了 1 个测试套， 10 个测试用例，其中

+ 8 个测试用例成功
+ 2 个测试用例失败

### 测试结论

发现一个问题，关联 issue [#45](https://github.com/ruyisdk/ruyi/issues/45)

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | oe_test_ruyi | 基本命令测试 |
|  | oe_test_ruyi_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | oe_test_ruyi_venv | ``venv`` 命令测试 |
|  | oe_test_ruyi_admin | ``admin`` 命令测试 |
|  | oe_test_ruyi_cmake_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | oe_test_ruyi_qemu | QEMU 支持测试 |
|  | oe_test_ruyi_xthead_qemu | 平头哥 QEMU 支持测试 |
|  | oe_test_ruyi_llvm | LLVM 支持测试 |
|  | oe_test_ruyi_news | ``news`` 命令测试 |
|  | oe_test_ruyi_device | ``device`` 命令测试 |

### 失败用例列表

| 测试用例名 | 状态 | 日志文件 | 原因 |
| :-: | :-: | :-: | :-: |
| oe_test_ruyi | fail | [log](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240116/logs/ubuntu2204-x86_64-qemu/oe_test_ruyi/2024-01-18-14_19_07.log) | ``ruyi self uninstall --purge`` 没有删除 ``.local/state/ruyi`` 目录 |
| oe_test_ruyi_xdg | fail | [log](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240116/logs/ubuntu2204-x86_64-qemu/oe_test_ruyi_xdg/2024-01-18-14_04_35.log) | ``ruyi self uninstall --purge`` 没有删除 ``"$XDG_STATE_DIR"/ruyi`` 目录 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240116/logs/jenkins.log)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240116/logs/ubuntu2204-x86_64-qemu/)
+ 视频录像，由于测试总时长达 4 小时，视频只演示测试触发和测试产物 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240116/videos/gitee-jenkins-test.mp4)

