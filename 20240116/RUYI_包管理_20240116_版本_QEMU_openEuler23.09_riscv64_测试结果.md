# RUYI 包管理 20240116 版本 QEMU openEuler 23.09 riscv64 测试结果

本次测试基于 RUYI 20240116 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64.20240116](https://mirror.iscas.ac.cn/ruyisdk/ruyi/testing/ruyi.riscv64.20240116) 。

编写了 mugen 测试用例，在 QEMU openEuler 23.09 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect 、 psmisc 、 ping 、 make 、 python3 、 python3-paramiko 、 python3-six ，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ openEuler 2309 riscv64 镜像使用软件所提供的镜像 [openEuler 2309 riscv64 base](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/20231130/v0.2/QEMU/openEuler-23.09-V1-base-qemu-testing.qcow2.zst)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ bash mugen.sh -f ruyi -x
Thu Jan 18 21:59:34 2024 - INFO  - start to run testcase:oe_test_ruyi_device.
Thu Jan 18 22:03:08 2024 - INFO  - The case exit by code 0.
Thu Jan 18 22:03:09 2024 - INFO  - End to run testcase:oe_test_ruyi_device.
Thu Jan 18 22:03:11 2024 - INFO  - start to run testcase:oe_test_ruyi_news.
Thu Jan 18 22:07:14 2024 - INFO  - The case exit by code 0.
Thu Jan 18 22:07:15 2024 - INFO  - End to run testcase:oe_test_ruyi_news.
Thu Jan 18 22:07:18 2024 - INFO  - start to run testcase:oe_test_ruyi_llvm.
Thu Jan 18 22:11:38 2024 - INFO  - The case exit by code 0.
Thu Jan 18 22:11:40 2024 - INFO  - End to run testcase:oe_test_ruyi_llvm.
Thu Jan 18 22:11:42 2024 - INFO  - start to run testcase:oe_test_ruyi_xdg.
Thu Jan 18 22:25:16 2024 - ERROR - The case exit by code 1.
Thu Jan 18 22:25:17 2024 - INFO  - End to run testcase:oe_test_ruyi_xdg.
Thu Jan 18 22:25:19 2024 - INFO  - start to run testcase:oe_test_ruyi_admin.
Thu Jan 18 22:32:16 2024 - INFO  - The case exit by code 0.
Thu Jan 18 22:32:17 2024 - INFO  - End to run testcase:oe_test_ruyi_admin.
Thu Jan 18 22:32:20 2024 - INFO  - start to run testcase:oe_test_ruyi_venv.
Thu Jan 18 22:42:36 2024 - INFO  - The case exit by code 0.
Thu Jan 18 22:42:37 2024 - INFO  - End to run testcase:oe_test_ruyi_venv.
Thu Jan 18 22:42:40 2024 - INFO  - start to run testcase:oe_test_ruyi.
Thu Jan 18 22:58:57 2024 - ERROR - The case exit by code 1.
Thu Jan 18 22:58:58 2024 - INFO  - End to run testcase:oe_test_ruyi.
Thu Jan 18 23:21:41 2024 - INFO  - End to run testcase:oe_test_ruyi_cmake_ninja.
Thu Jan 18 23:21:43 2024 - INFO  - start to run testcase:oe_test_ruyi_xthead_qemu.
Thu Jan 18 23:26:47 2024 - INFO  - The case exit by code 0.
Thu Jan 18 23:26:49 2024 - INFO  - End to run testcase:oe_test_ruyi_xthead_qemu.
Thu Jan 18 23:26:51 2024 - INFO  - start to run testcase:oe_test_ruyi_qemu.
Thu Jan 18 23:30:41 2024 - INFO  - The case exit by code 0.
Thu Jan 18 23:30:42 2024 - INFO  - End to run testcase:oe_test_ruyi_qemu.
Thu Jan 18 23:30:43 2024 - INFO  - A total of 10 use cases were executed, with 8 successes and 2 failures.
```

### 测试结果

共测试了 1 个测试套， 10 个测试用例，其中

+ 8 个测试用例成功
+ 2 个测试用例失败

### 测试结论

发现一个问题，关联 issue [#45](https://github.com/ruyisdk/ruyi/issues/45)

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | oe_test_ruyi | 基本命令测试 |
|  | oe_test_ruyi_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | oe_test_ruyi_venv | ``venv`` 命令测试 |
|  | oe_test_ruyi_admin | ``admin`` 命令测试 |
|  | oe_test_ruyi_cmake_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | oe_test_ruyi_qemu | QEMU 支持测试 |
|  | oe_test_ruyi_xthead_qemu | 平头哥 QEMU 支持测试 |
|  | oe_test_ruyi_llvm | LLVM 支持测试 |
|  | oe_test_ruyi_news | ``news`` 命令测试 |
|  | oe_test_ruyi_device | ``device`` 命令测试 |

### 失败用例列表

| 测试用例名 | 状态 | 日志文件 | 原因 |
| :-: | :-: | :-: | :-: |
| oe_test_ruyi | fail | [log](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240116/logs/oE2309-riscv64-qemu/oe_test_ruyi/2024-01-18-22_42_40.log) | ``ruyi self uninstall --purge`` 没有删除 ``.local/state/ruyi`` 目录 |
| oe_test_ruyi_xdg | fail | [log](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240116/logs/oE2309-riscv64-qemu/oe_test_ruyi_xdg/2024-01-18-22_11_43.log) | ``ruyi self uninstall --purge`` 没有删除 ``"$XDG_STATE_DIR"/ruyi`` 目录 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240116/logs/jenkins.log)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240116/logs/oE2309-riscv64-qemu/)
+ 视频录像，由于测试总时长达 4 小时，视频只演示测试触发和测试产物 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240116/videos/gitee-jenkins-test.mp4)

