# RUYI 包管理 20240116 版本镜像刷写测试报告

本次测试基于 RUYI 20240116 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64.20240116](https://mirror.iscas.ac.cn/ruyisdk/ruyi/testing/ruyi.riscv64.20240116)。

由于涉及到硬盘镜像写入和开发板镜像烧录，本次测试手动进行。

### 测试环境说明

+ 测试环境为 Ubuntu 22.04.3 LTS x86_64。

### 测试执行

0. 安装 ruyi CLI: `sudo curl -Lo /usr/bin/ruyi https://mirror.iscas.ac.cn/ruyisdk/ruyi/testing/ruyi.amd64.20240116 && sudo chmod +x /usr/bin/ruyi`

1. 安装 `fastboot`。

```bash
sudo apt install -y fastboot
```
2. 添加 LPi4A 的 `udev` 规则。

```conf
# /etc/udev/rules.d/51-android.conf
SUBSYSTEM=="usb", ATTR{idVendor}=="1234", MODE="0660", GROUP="adbusers", TAG+="uaccess"
SUBSYSTEM=="usb", ATTR{idVendor}=="1234", ATTR{idProduct}=="8888", SYMLINK+="android_fastboot"
SUBSYSTEM=="usb", ATTR{idVendor}=="2345", MODE="0660", GROUP="adbusers", TAG+="uaccess"
SUBSYSTEM=="usb", ATTR{idVendor}=="2345", ATTR{idProduct}=="7654", SYMLINK+="android_fastboot"
```

3. **重载**并**触发** udev 规则：

```bash
sudo udevadm control --reload-rules
sudo udevadm trigger
```

4. 运行 `ruyi device provition`。

5. 按提示进行操作。

6. 查看镜像是否正确写入，检查开发板能否启动。

### 测试结果

LPi4A 16GB 版本的镜像能够被 fastboot 正常写入且正常启动；

Pioneer 镜像能被正确写入到外置存储设备上，经单独测试，系统正常启动。

### 测试结论

ruyi 20240116 版本的 device provision 功能通过测试。

### 测试用例列表

|          测试用例名          |            测试内容            |
|:------------------------:|:--------------------------:|
|     LPi4A 16GB 镜像刷写      | 测试 fastboot 镜像刷写是否正常 |
| Milk-V Pioneer v1.3 镜像刷写 |      测试 dd 镜像是否正常      |


### 失败用例列表

| 测试环境 | 测试用例名 | 状态 | 日志文件 | 原因 |
| :-: | :-: | :-: | :-: | :-: |

### 其他信息

在 20240116 版本中，fastboot 需先行配置 udev rule；由于后续版本将使用 `sudo` 以特权用户身份执行 `fastboot`，将不再需要手动配置 `udev` 规则。

> 关联 issue：[ruyisdk/ruyi #39](https://github.com/ruyisdk/ruyi/issues/39)

屏幕录像可查看：

#### LPi4A

[![LPi4A](https://asciinema.org/a/3vIlAezs1xJmHm0Et2Wq0uzDI.svg)](https://asciinema.org/a/3vIlAezs1xJmHm0Et2Wq0uzDI?autoplay=1)

#### Milk-V Pioneer v1.3

[![Milk-V Pioneer v1.3](https://asciinema.org/a/PR6WS7vjgMY2TId2EjaDjQb7a.svg)](https://asciinema.org/a/PR6WS7vjgMY2TId2EjaDjQb7a?autoplay=1)

视频中亦演示了 `ruyi news` 功能的使用。