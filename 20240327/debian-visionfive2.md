# ruyi 包管理 20240326 版本 测试报告

本次测试基于 `ruyi` 0.7.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.7.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 Debian bookworm/sid (StarFive VisionFive 2, v1.3B) 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 `expect`、`psmisc`、`ping`、`make`、`python3`、`python3-paramiko`、`python3-six`，故 `mugen` 自动化测试不能测试 `ruyi` 对这些软件包的依赖。
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的。
+ 测试环境为 Debian bookworm/sid，系统镜像为 [StarFive 厂商镜像](https://debian.starfivetech.com/)，镜像文件名为：`starfive-jh7110-202403-nvme-minimal-desktop-wayland.img.bz2`。
+ 系统安装在 NVMe SSD 上，为了从 NVMe SSD 启动系统，需要更新 SPL 和 U-Boot 至最新版本，并将启动模式切换至 SPI。
    + 详见 RVspace 提供的 [教程](https://doc.rvspace.org/VisionFive2/Quick_Start_Guide/VisionFive2_SDK_QSG/updating_spl_and_u_boot%20-%20vf2.html)。
+ StarFive 提供的 Debian 镜像默认禁用了 SSH `root` 登录，需要手动开启后重启 SSH Daemon，参见 [RVspace](https://doc.rvspace.org/VisionFive2/Quick_Start_Guide/VisionFive2_QSG/enable_ssh_root_login.html)：

```shell
echo 'PermitRootLogin=yes'  | sudo tee -a /etc/ssh/sshd_config
sudo systemctl restart sshd
```

### 测试执行

0. 确保已参考上文开启 `sshd` `root` 用户登录。

1. 以 `root` 用户身份登录系统。

2. 执行自动化测试脚本：`curl -LO https://github.com/KevinMX/PLCT-Tarsier-Works/raw/main/misc/scripts/mugen_ruyi.sh && bash mugen_ruyi.sh starfive`

### 测试结果

共测试了 1 个测试套，13 个测试用例，13 个测试用例成功，0 个测试用例失败。

### 测试用例列表

| 测试套/软件包名 |             测试用例名             |                 测试内容                  |
|:---------------:|:----------------------------------:|:-------------------------------------:|
|      ruyi       |         ruyi\_test\_common         |               基本命令测试                |
|                 |          ruyi\_test\_xdg           |        ``XDG_*_HOME`` 环境变量测试        |
|                 |          ruyi\_test\_venv          |             ``venv`` 命令测试             |
|                 |         ruyi\_test\_admin          |            ``admin`` 命令测试             |
|                 |      ruyi\_test\_cmake\_ninja      | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|                 |          ruyi\_test\_qemu          |               QEMU 支持测试               |
|                 |      ruyi\_test\_xthead\_qemu      |           平头哥 QEMU 支持测试            |
|                 |          ruyi\_test\_llvm          |               LLVM 支持测试               |
|                 |          ruyi\_test\_news          |             ``news`` 命令测试             |
|                 |         ruyi\_test\_device         |            ``device`` 命令测试            |
|                 | ruyi\_test\_gnu-plct-rv64ilp32-elf |   ``gnu-plct-rv64ilp32-elf`` 工具链测试   |
|                 |         ruyi\_test\_config         |         config.toml 配置文件测试          |
|                 |        ruyi\_test\_binaries        |               二进制包测试                |

### 其他信息

详细日志见 [debian-bookworm_sid-riscv64-visionfive2](/20240327/logs/debian-bookworm_sid-riscv64-visionfive2/) 目录。

```log
Wed Mar 27 07:25:55 2024 - INFO  - 配置文件加载完成...
Wed Mar 27 07:25:56 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Wed Mar 27 07:26:28 2024 - INFO  - The case exit by code 0.
Wed Mar 27 07:26:28 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Wed Mar 27 07:26:29 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Mar 27 07:27:25 2024 - INFO  - The case exit by code 0.
Wed Mar 27 07:27:25 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Mar 27 07:27:25 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Wed Mar 27 07:27:56 2024 - INFO  - The case exit by code 0.
Wed Mar 27 07:27:56 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Wed Mar 27 07:27:56 2024 - INFO  - start to run testcase:ruyi_test_admin.
Wed Mar 27 07:28:18 2024 - INFO  - The case exit by code 0.
Wed Mar 27 07:28:18 2024 - INFO  - End to run testcase:ruyi_test_admin.
Wed Mar 27 07:28:19 2024 - INFO  - start to run testcase:ruyi_test_device.
Wed Mar 27 07:33:40 2024 - INFO  - The case exit by code 0.
Wed Mar 27 07:33:40 2024 - INFO  - End to run testcase:ruyi_test_device.
Wed Mar 27 07:33:40 2024 - INFO  - start to run testcase:ruyi_test_common.
Wed Mar 27 07:38:13 2024 - INFO  - The case exit by code 0.
Wed Mar 27 07:38:13 2024 - INFO  - End to run testcase:ruyi_test_common.
Wed Mar 27 07:38:13 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Wed Mar 27 07:44:42 2024 - INFO  - The case exit by code 0.
Wed Mar 27 07:44:42 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Wed Mar 27 07:44:43 2024 - INFO  - start to run testcase:ruyi_test_news.
Wed Mar 27 07:45:26 2024 - INFO  - The case exit by code 0.
Wed Mar 27 07:45:27 2024 - INFO  - End to run testcase:ruyi_test_news.
Wed Mar 27 07:45:27 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Wed Mar 27 07:48:36 2024 - INFO  - The case exit by code 0.
Wed Mar 27 07:48:36 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Wed Mar 27 07:48:36 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Wed Mar 27 08:00:47 2024 - INFO  - The case exit by code 0.
Wed Mar 27 08:00:47 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Wed Mar 27 08:00:48 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Wed Mar 27 08:04:01 2024 - INFO  - The case exit by code 0.
Wed Mar 27 08:04:02 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Wed Mar 27 08:04:02 2024 - INFO  - start to run testcase:ruyi_test_config.
Wed Mar 27 08:04:38 2024 - INFO  - The case exit by code 0.
Wed Mar 27 08:04:38 2024 - INFO  - End to run testcase:ruyi_test_config.
Wed Mar 27 08:04:39 2024 - INFO  - start to run testcase:ruyi_test_venv.
Wed Mar 27 08:07:00 2024 - INFO  - The case exit by code 0.
Wed Mar 27 08:07:00 2024 - INFO  - End to run testcase:ruyi_test_venv.
Wed Mar 27 08:07:00 2024 - INFO  - A total of 13 use cases were executed, with 13 successes and 0 failures.
```