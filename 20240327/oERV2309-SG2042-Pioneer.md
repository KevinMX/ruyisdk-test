# ruyi 包管理 20240326 版本 测试报告

本次测试基于 `ruyi` 0.7.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.7.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 openEuler 23.09 RISC-V preview V1 (Milk-V Pioneer v1.3) 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 `expect`、`psmisc`、`ping`、`make`、`python3`、`python3-paramiko`、`python3-six`，故 `mugen` 自动化测试不能测试 `ruyi` 对这些软件包的依赖。
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的。
+ 测试环境为 openEuler 23.09 RISC-V preview V1。

### 测试执行

0. 以 `root` 用户身份登录系统。

1. 执行自动化测试脚本：`curl -LO https://github.com/KevinMX/PLCT-Tarsier-Works/raw/main/misc/scripts/mugen_ruyi.sh && bash mugen_ruyi.sh openEuler12#$`

### 测试结果

共测试了 1 个测试套，13 个测试用例，13 个测试用例成功，0 个测试用例失败。

### 测试用例列表

| 测试套/软件包名 |             测试用例名             |                 测试内容                  |
|:---------------:|:----------------------------------:|:-------------------------------------:|
|      ruyi       |         ruyi\_test\_common         |               基本命令测试                |
|                 |          ruyi\_test\_xdg           |        ``XDG_*_HOME`` 环境变量测试        |
|                 |          ruyi\_test\_venv          |             ``venv`` 命令测试             |
|                 |         ruyi\_test\_admin          |            ``admin`` 命令测试             |
|                 |      ruyi\_test\_cmake\_ninja      | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|                 |          ruyi\_test\_qemu          |               QEMU 支持测试               |
|                 |      ruyi\_test\_xthead\_qemu      |           平头哥 QEMU 支持测试            |
|                 |          ruyi\_test\_llvm          |               LLVM 支持测试               |
|                 |          ruyi\_test\_news          |             ``news`` 命令测试             |
|                 |         ruyi\_test\_device         |            ``device`` 命令测试            |
|                 | ruyi\_test\_gnu-plct-rv64ilp32-elf |   ``gnu-plct-rv64ilp32-elf`` 工具链测试   |
|                 |         ruyi\_test\_config         |         config.toml 配置文件测试          |
|                 |        ruyi\_test\_binaries        |               二进制包测试                |

### 其他信息

详细日志见 [oE2309-riscv64-sg2042](/20240327/logs/oE2309-riscv64-sg2042/) 目录。

```log
Wed Mar 27 14:53:45 2024 - INFO  - 配置文件加载完成...                                                                                                                   
Wed Mar 27 14:53:46 2024 - INFO  - start to run testcase:ruyi_test_device.                                                                                               
Wed Mar 27 14:58:44 2024 - INFO  - The case exit by code 0.                                                                                                              
Wed Mar 27 14:58:44 2024 - INFO  - End to run testcase:ruyi_test_device.                                                                                                 
Wed Mar 27 14:58:45 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.                                                                                          
Wed Mar 27 15:04:55 2024 - INFO  - The case exit by code 0.                                                                                                              
Wed Mar 27 15:04:55 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.                                                                                            
Wed Mar 27 15:04:55 2024 - INFO  - start to run testcase:ruyi_test_config.                                                                                               
Wed Mar 27 15:05:20 2024 - INFO  - The case exit by code 0.                                                                                                              
Wed Mar 27 15:05:20 2024 - INFO  - End to run testcase:ruyi_test_config.                                                                                                 
Wed Mar 27 15:05:21 2024 - INFO  - start to run testcase:ruyi_test_common.                                                                                               
Wed Mar 27 15:08:07 2024 - INFO  - The case exit by code 0.                                                                                                              
Wed Mar 27 15:08:08 2024 - INFO  - End to run testcase:ruyi_test_common.                                                                                                 
Wed Mar 27 15:08:08 2024 - INFO  - start to run testcase:ruyi_test_news.                                                                                                 
Wed Mar 27 15:08:38 2024 - INFO  - The case exit by code 0.                         
Wed Mar 27 15:08:38 2024 - INFO  - End to run testcase:ruyi_test_news.              
Wed Mar 27 15:08:39 2024 - INFO  - start to run testcase:ruyi_test_qemu.                                                                                                 
Wed Mar 27 15:08:58 2024 - INFO  - The case exit by code 0.                                                                                                              
Wed Mar 27 15:08:58 2024 - INFO  - End to run testcase:ruyi_test_qemu.              
Wed Mar 27 15:08:59 2024 - INFO  - start to run testcase:ruyi_test_llvm.                                                                                                 
Wed Mar 27 15:09:17 2024 - INFO  - The case exit by code 0.                                                                                                              
Wed Mar 27 15:09:17 2024 - INFO  - End to run testcase:ruyi_test_llvm.                                                                                                   
Wed Mar 27 15:09:18 2024 - INFO  - start to run testcase:ruyi_test_venv.                                                                                                 
Wed Mar 27 15:10:53 2024 - INFO  - The case exit by code 0.                                                                                                              
Wed Mar 27 15:10:54 2024 - INFO  - End to run testcase:ruyi_test_venv.                                                                                                   
Wed Mar 27 15:10:54 2024 - INFO  - start to run testcase:ruyi_test_admin.           
Wed Mar 27 15:11:07 2024 - INFO  - The case exit by code 0.                         
Wed Mar 27 15:11:07 2024 - INFO  - End to run testcase:ruyi_test_admin.                                                                                                  
Wed Mar 27 15:11:07 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.     
Wed Mar 27 15:13:15 2024 - INFO  - The case exit by code 0.                         
Wed Mar 27 15:13:15 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Wed Mar 27 15:13:16 2024 - INFO  - start to run testcase:ruyi_test_xdg.             
Wed Mar 27 15:15:10 2024 - INFO  - The case exit by code 0.                 
Wed Mar 27 15:15:10 2024 - INFO  - End to run testcase:ruyi_test_xdg.      
Wed Mar 27 15:15:11 2024 - INFO  - start to run testcase:ruyi_test_binaries. 
Wed Mar 27 15:19:55 2024 - INFO  - The case exit by code 0.                 
Wed Mar 27 15:19:55 2024 - INFO  - End to run testcase:ruyi_test_binaries.     
Wed Mar 27 15:19:55 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Mar 27 15:20:32 2024 - INFO  - The case exit by code 0.                                                                                                              
Wed Mar 27 15:20:32 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Mar 27 15:20:32 2024 - INFO  - A total of 13 use cases were executed, with 13 successes and 0 failures.
```