# RUYI 包管理 Container Archlinux riscv64 测试结果

本次测试基于 RUYI 0.7.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.7.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 LXC Archlinux riscv64 环境开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ Archlinux riscv64 测试环境来自发布的 [rootfs](https://archriscv.felixc.at/images/archriscv-2023-12-13.tar.zst)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Wed Mar 27 03:12:20 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Wed Mar 27 03:12:54 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:12:54 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Wed Mar 27 03:12:55 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Wed Mar 27 03:13:31 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:13:31 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Wed Mar 27 03:13:31 2024 - INFO  - start to run testcase:ruyi_test_common.
Wed Mar 27 03:26:08 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:26:08 2024 - INFO  - End to run testcase:ruyi_test_common.
Wed Mar 27 03:26:08 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Wed Mar 27 03:37:04 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:37:04 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Wed Mar 27 03:37:05 2024 - INFO  - start to run testcase:ruyi_test_device.
Wed Mar 27 03:42:35 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:42:35 2024 - INFO  - End to run testcase:ruyi_test_device.
Wed Mar 27 03:42:36 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Wed Mar 27 03:46:41 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:46:41 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Wed Mar 27 03:46:42 2024 - INFO  - start to run testcase:ruyi_test_news.
Wed Mar 27 03:47:30 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:47:30 2024 - INFO  - End to run testcase:ruyi_test_news.
Wed Mar 27 03:47:31 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Mar 27 03:48:30 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:48:30 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Mar 27 03:48:30 2024 - INFO  - start to run testcase:ruyi_test_venv.
Wed Mar 27 03:51:12 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:51:12 2024 - INFO  - End to run testcase:ruyi_test_venv.
Wed Mar 27 03:51:12 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Wed Mar 27 03:55:13 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:55:13 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Wed Mar 27 03:55:13 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Wed Mar 27 04:04:54 2024 - INFO  - The case exit by code 0.
Wed Mar 27 04:04:54 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Wed Mar 27 04:04:55 2024 - INFO  - start to run testcase:ruyi_test_config.
Wed Mar 27 04:05:34 2024 - INFO  - The case exit by code 0.
Wed Mar 27 04:05:34 2024 - INFO  - End to run testcase:ruyi_test_config.
Wed Mar 27 04:05:34 2024 - INFO  - start to run testcase:ruyi_test_admin.
Wed Mar 27 04:05:54 2024 - INFO  - The case exit by code 0.
Wed Mar 27 04:05:54 2024 - INFO  - End to run testcase:ruyi_test_admin.
Wed Mar 27 04:05:54 2024 - INFO  - A total of 13 use cases were executed, with 13 successes and 0 failures.
```

### 测试结果

共测试了 1 个测试套， 13 个测试用例，其中

+ 13 个测试用例成功
+ 0 个测试用例失败
+ 0 个测试用例超时

### 测试结论

没有发现问题

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240327/logs/jenkins/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240327/logs/jenkins/0.7.0_test.mp4)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240327/logs/archlinux_riscv64_container/)
