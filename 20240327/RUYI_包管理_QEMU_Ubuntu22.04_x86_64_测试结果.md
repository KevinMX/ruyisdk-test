# RUYI 包管理 QEMU Ubuntu 22.04 x86\_64 测试结果

本次测试基于 RUYI 0.7.0 版本预编译的 amd64 架构版本二进制 [ruyi.amd64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.7.0/ruyi.amd64) 。

编写了 mugen 测试用例，在 QEMU ubuntu 22.04 x86\_64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ Ubuntu 22.04 LTS 镜像使用 Ubuntu 提供的 QEMU 镜像 [ubuntu-22.04-server-cloudimg-amd64](https://cloud-images.ubuntu.com/releases/jammy/release/ubuntu-22.04-server-cloudimg-amd64-disk-kvm.img)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Wed Mar 27 03:15:20 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Mar 27 03:18:35 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:18:35 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Mar 27 03:18:35 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Wed Mar 27 03:31:15 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:31:15 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Wed Mar 27 03:31:15 2024 - INFO  - start to run testcase:ruyi_test_news.
Wed Mar 27 03:33:00 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:33:00 2024 - INFO  - End to run testcase:ruyi_test_news.
Wed Mar 27 03:33:01 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Wed Mar 27 03:49:19 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:49:19 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Wed Mar 27 03:49:19 2024 - INFO  - start to run testcase:ruyi_test_common.
Wed Mar 27 03:54:27 2024 - INFO  - The case exit by code 0.
Wed Mar 27 03:54:27 2024 - INFO  - End to run testcase:ruyi_test_common.
Wed Mar 27 03:54:28 2024 - INFO  - start to run testcase:ruyi_test_device.
Wed Mar 27 04:01:08 2024 - INFO  - The case exit by code 0.
Wed Mar 27 04:01:08 2024 - INFO  - End to run testcase:ruyi_test_device.
Wed Mar 27 04:01:08 2024 - INFO  - start to run testcase:ruyi_test_config.
Wed Mar 27 04:05:07 2024 - INFO  - The case exit by code 0.
Wed Mar 27 04:05:07 2024 - INFO  - End to run testcase:ruyi_test_config.
Wed Mar 27 04:05:08 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Wed Mar 27 04:19:07 2024 - INFO  - The case exit by code 0.
Wed Mar 27 04:19:07 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Wed Mar 27 04:19:07 2024 - INFO  - start to run testcase:ruyi_test_venv.
Wed Mar 27 04:23:11 2024 - INFO  - The case exit by code 0.
Wed Mar 27 04:23:11 2024 - INFO  - End to run testcase:ruyi_test_venv.
Wed Mar 27 04:23:11 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Wed Mar 27 04:37:48 2024 - INFO  - The case exit by code 0.
Wed Mar 27 04:37:48 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Wed Mar 27 04:37:48 2024 - INFO  - start to run testcase:ruyi_test_admin.
Wed Mar 27 04:38:18 2024 - INFO  - The case exit by code 0.
Wed Mar 27 04:38:18 2024 - INFO  - End to run testcase:ruyi_test_admin.
Wed Mar 27 04:38:18 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Wed Mar 27 04:42:21 2024 - INFO  - The case exit by code 0.
Wed Mar 27 04:42:21 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Wed Mar 27 04:42:21 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Wed Mar 27 04:46:19 2024 - INFO  - The case exit by code 0.
Wed Mar 27 04:46:19 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Wed Mar 27 04:46:19 2024 - INFO  - A total of 13 use cases were executed, with 13 successes and 0 failures.
```

### 测试结果

共测试了 1 个测试套， 13 个测试用例，其中

+ 13 个测试用例成功
+ 0 个测试用例失败
+ 0 个测试用例超时

### 测试结论

没有发现问题

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240327/logs/jenkins/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240327/logs/jenkins/0.7.0_test.mp4)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240327/logs/ubuntu2204-x86_64-qemu/)
