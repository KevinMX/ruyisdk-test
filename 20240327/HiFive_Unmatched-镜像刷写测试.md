# RUYI 包管理 0.7.0 版本 HiFive Unmatched 镜像刷写测试结果

本次测试基于 ruyi 0.7.0 版本预编译的 amd64 架构版本二进制 [ruyi.amd64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.7.0/ruyi.amd64)。

由于涉及到硬盘镜像写入和开发板镜像烧录，本次测试手动进行。

### 测试环境说明

+ 测试环境为 Ubuntu 22.04.4 LTS x86_64。

### 测试执行

0. 安装 ruyi CLI: `sudo curl -Lo /usr/bin/ruyi https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.7.0/ruyi.riscv64 && sudo chmod +x /usr/bin/ruyi`
1. 运行 `ruyi device provision`。
2. 按提示操作，分别刷入 FreeBSD 和 OpenBSD 镜像进行测试。
3. 将 U-Boot 烧录进 microSD 卡或 SPI，并切换对应的启动模式。详情可参阅 [OpenBSD 文档](https://ftp.openbsd.org/pub/OpenBSD/snapshots/riscv64/INSTALL.riscv64)。
4. 使用 `eject` 命令弹出 U 盘，插入开发板，连接串口，检查是否正常启动。

### 测试结果

HiFive Unmatched 的 FreeBSD 和 OpenBSD 镜像能够被正常写入且正常启动。

### 测试结论

ruyi 0.7.0 amd64 版本的 `device provision` 功能通过测试。

### 测试用例列表

|        测试用例名         |       测试内容       |
|:---------------------:|:----------------:|
| HiFive Unmatched 镜像刷写 | 测试 dd 镜像是否正常 |

### 失败用例列表

| 测试环境 | 测试用例名 | 状态 | 日志文件 | 原因 |
| :-: | :-: | :-: | :-: | :-: |

### 其他信息

屏幕录像如下。

（包括了刷写和开发板启动时的串口输出）

#### Unmatched 启动日志

FreeBSD:

[![asciicast](https://asciinema.org/a/pW22serGYd1MUZ7HwZW8PrPeN.svg)](https://asciinema.org/a/pW22serGYd1MUZ7HwZW8PrPeN)

OpenBSD:

[![asciicast](https://asciinema.org/a/9iTm9mqhKhOE0lYy2E075cBll.svg)](https://asciinema.org/a/9iTm9mqhKhOE0lYy2E075cBll)