# RUYI 包管理 Container Archlinux x86\_64 测试结果

本次测试基于 RUYI 0.7.0 版本预编译的 amd64 架构版本二进制 [ruyi.amd64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.7.0/ruyi.amd64) 。

编写了 mugen 测试用例，在 LXC Archlinux x86\_64 环境开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ Archlinux x86\_64 测试环境为使用 ``pacstrap`` 制作的容器

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Tue Mar 26 11:34:41 2024 - INFO  - start to run testcase:ruyi_test_venv.
Tue Mar 26 11:35:08 2024 - INFO  - The case exit by code 0.
Tue Mar 26 11:35:08 2024 - INFO  - End to run testcase:ruyi_test_venv.
Tue Mar 26 11:35:08 2024 - INFO  - start to run testcase:ruyi_test_config.
Tue Mar 26 11:35:20 2024 - INFO  - The case exit by code 0.
Tue Mar 26 11:35:20 2024 - INFO  - End to run testcase:ruyi_test_config.
Tue Mar 26 11:35:20 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Tue Mar 26 11:35:57 2024 - INFO  - The case exit by code 0.
Tue Mar 26 11:35:57 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Tue Mar 26 11:35:57 2024 - INFO  - start to run testcase:ruyi_test_common.
Tue Mar 26 11:36:35 2024 - INFO  - The case exit by code 0.
Tue Mar 26 11:36:35 2024 - INFO  - End to run testcase:ruyi_test_common.
Tue Mar 26 11:36:35 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Tue Mar 26 11:38:26 2024 - INFO  - The case exit by code 0.
Tue Mar 26 11:38:26 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Tue Mar 26 11:38:26 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Tue Mar 26 11:40:06 2024 - INFO  - The case exit by code 0.
Tue Mar 26 11:40:06 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Tue Mar 26 11:40:06 2024 - INFO  - start to run testcase:ruyi_test_admin.
Tue Mar 26 11:40:10 2024 - INFO  - The case exit by code 0.
Tue Mar 26 11:40:10 2024 - INFO  - End to run testcase:ruyi_test_admin.
Tue Mar 26 11:40:10 2024 - INFO  - start to run testcase:ruyi_test_device.
Tue Mar 26 11:44:31 2024 - INFO  - The case exit by code 0.
Tue Mar 26 11:44:31 2024 - INFO  - End to run testcase:ruyi_test_device.
Tue Mar 26 11:44:31 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Tue Mar 26 11:45:06 2024 - INFO  - The case exit by code 0.
Tue Mar 26 11:45:06 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Tue Mar 26 11:45:06 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Tue Mar 26 11:45:52 2024 - INFO  - The case exit by code 0.
Tue Mar 26 11:45:52 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Tue Mar 26 11:45:52 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Tue Mar 26 11:46:03 2024 - INFO  - The case exit by code 0.
Tue Mar 26 11:46:03 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Tue Mar 26 11:46:03 2024 - INFO  - start to run testcase:ruyi_test_news.
Tue Mar 26 11:46:11 2024 - INFO  - The case exit by code 0.
Tue Mar 26 11:46:11 2024 - INFO  - End to run testcase:ruyi_test_news.
Tue Mar 26 11:46:11 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Tue Mar 26 11:46:49 2024 - INFO  - The case exit by code 0.
Tue Mar 26 11:46:49 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Tue Mar 26 11:46:49 2024 - INFO  - A total of 13 use cases were executed, with 13 successes and 0 failures.
```

### 测试结果

共测试了 1 个测试套， 13 个测试用例，其中

+ 13 个测试用例成功
+ 0 个测试用例失败
+ 0 个测试用例超时

### 测试结论

没有发现问题

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240327/logs/jenkins/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240327/logs/jenkins/0.7.0_test.mp4)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240327/logs/archlinux_x86_64_container/)
