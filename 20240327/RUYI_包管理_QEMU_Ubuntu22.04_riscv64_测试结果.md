# RUYI 包管理 QEMU Ubuntu 22.04 riscv64 测试结果

本次测试基于 RUYI 0.7.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.7.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 QEMU ubuntu 22.04 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ Ubuntu 22.04 LTS 镜像使用 Ubuntu 提供的 Unmatched 镜像 [ubuntu-22.04.4-preinstalled-server-riscv64\_unmatched](https://mirrors.bfsu.edu.cn/ubuntu-cdimage/releases/22.04.4/release/ubuntu-22.04.4-preinstalled-server-riscv64%2Bunmatched.img.xz)
+ 在官方文档中 QEMU 与 Unmatched 共用一个镜像

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Wed Mar 27 05:59:32 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Wed Mar 27 06:16:47 2024 - INFO  - The case exit by code 0.
Wed Mar 27 06:16:49 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Wed Mar 27 06:16:52 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Wed Mar 27 06:22:49 2024 - INFO  - The case exit by code 0.
Wed Mar 27 06:22:51 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Wed Mar 27 06:22:54 2024 - INFO  - start to run testcase:ruyi_test_venv.
Wed Mar 27 06:37:21 2024 - INFO  - The case exit by code 0.
Wed Mar 27 06:37:23 2024 - INFO  - End to run testcase:ruyi_test_venv.
Wed Mar 27 06:37:26 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Wed Mar 27 06:54:20 2024 - INFO  - The case exit by code 0.
Wed Mar 27 06:54:22 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Wed Mar 27 06:54:25 2024 - INFO  - start to run testcase:ruyi_test_news.
Wed Mar 27 07:01:29 2024 - INFO  - The case exit by code 0.
Wed Mar 27 07:01:31 2024 - INFO  - End to run testcase:ruyi_test_news.
Wed Mar 27 07:01:34 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Wed Mar 27 07:43:48 2024 - INFO  - The case exit by code 0.
Wed Mar 27 07:43:50 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Wed Mar 27 07:43:53 2024 - INFO  - start to run testcase:ruyi_test_config.
Wed Mar 27 07:49:41 2024 - INFO  - The case exit by code 0.
Wed Mar 27 07:49:43 2024 - INFO  - End to run testcase:ruyi_test_config.
Wed Mar 27 07:49:46 2024 - INFO  - start to run testcase:ruyi_test_admin.
Wed Mar 27 07:54:43 2024 - INFO  - The case exit by code 0.
Wed Mar 27 07:54:45 2024 - INFO  - End to run testcase:ruyi_test_admin.
Wed Mar 27 07:54:48 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Wed Mar 27 08:25:28 2024 - INFO  - The case exit by code 0.
Wed Mar 27 08:25:29 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Wed Mar 27 08:25:32 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Mar 27 08:32:34 2024 - INFO  - The case exit by code 0.
Wed Mar 27 08:32:36 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Wed Mar 27 08:32:39 2024 - INFO  - start to run testcase:ruyi_test_common.
Wed Mar 27 08:50:15 2024 - INFO  - The case exit by code 0.
Wed Mar 27 08:50:17 2024 - INFO  - End to run testcase:ruyi_test_common.
Wed Mar 27 08:50:20 2024 - INFO  - start to run testcase:ruyi_test_device.
Wed Mar 27 09:04:08 2024 - INFO  - The case exit by code 0.
Wed Mar 27 09:04:10 2024 - INFO  - End to run testcase:ruyi_test_device.
Wed Mar 27 09:04:13 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Wed Mar 27 09:09:53 2024 - INFO  - The case exit by code 0.
Wed Mar 27 09:09:54 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Wed Mar 27 09:09:56 2024 - INFO  - A total of 13 use cases were executed, with 13 successes and 0 failures.
```

### 测试结果

共测试了 1 个测试套， 13 个测试用例，其中

+ 13 个测试用例成功
+ 0 个测试用例失败
+ 0 个测试用例超时

### 测试结论

没有发现问题

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240327/logs/jenkins/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240327/logs/jenkins/0.7.0_test.mp4)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240327/logs/ubuntu2204-riscv64-qemu/)
