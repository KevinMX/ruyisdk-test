# RUYI 包管理 QEMU openKylin riscv64 测试结果

本次测试基于 RUYI 0.9.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.9.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 QEMU openKylin 1.0.1 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ openKylin 镜像使用了 [VisionFive 2 镜像](https://mirrors.wsyu.edu.cn/openkylin-cdimage/1.0.1/openKylin-1.0.1-visionfive2-riscv64.img.xz)，替换了来自 [unmatched 镜像](https://mirrors.wsyu.edu.cn/openkylin-cdimage/1.0/openKylin-1.0-hifive-unmatched-riscv64.img.xz)的 generic 内核。

### 测试流程

手动运行测试，共运行了两遍完整的测试。

### 测试日志

此为第二次测试的日志，其中 ``ruyi_test_venv`` 用例在第一次测试中通过。

```bash
+ sudo bash mugen.sh -f ruyi -x
Fri Apr 26 02:06:01 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Fri Apr 26 02:38:23 2024 - INFO  - The case exit by code 0.
Fri Apr 26 02:38:24 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Fri Apr 26 02:38:26 2024 - INFO  - start to run testcase:ruyi_test_admin.
Fri Apr 26 02:40:50 2024 - INFO  - The case exit by code 0.
Fri Apr 26 02:40:51 2024 - INFO  - End to run testcase:ruyi_test_admin.
Fri Apr 26 02:40:53 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Fri Apr 26 02:47:04 2024 - INFO  - The case exit by code 0.
Fri Apr 26 02:47:05 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Fri Apr 26 02:47:07 2024 - INFO  - start to run testcase:ruyi_test_venv.
Fri Apr 26 02:51:13 2024 - ERROR - The case exit by code 3.
Fri Apr 26 02:51:14 2024 - INFO  - End to run testcase:ruyi_test_venv.
Fri Apr 26 02:51:16 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Fri Apr 26 02:55:52 2024 - INFO  - The case exit by code 0.
Fri Apr 26 02:55:53 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Fri Apr 26 02:55:55 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Fri Apr 26 03:11:36 2024 - INFO  - The case exit by code 0.
Fri Apr 26 03:11:37 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Fri Apr 26 03:11:39 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Fri Apr 26 03:13:59 2024 - INFO  - The case exit by code 0.
Fri Apr 26 03:14:00 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Fri Apr 26 03:14:02 2024 - INFO  - start to run testcase:ruyi_test_device.
Fri Apr 26 03:17:25 2024 - INFO  - The case exit by code 0.
Fri Apr 26 03:17:26 2024 - INFO  - End to run testcase:ruyi_test_device.
Fri Apr 26 03:17:28 2024 - INFO  - start to run testcase:ruyi_test_config.
Fri Apr 26 03:20:07 2024 - INFO  - The case exit by code 0.
Fri Apr 26 03:20:08 2024 - INFO  - End to run testcase:ruyi_test_config.
Fri Apr 26 03:20:10 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Fri Apr 26 03:23:47 2024 - INFO  - The case exit by code 0.
Fri Apr 26 03:23:48 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Fri Apr 26 03:23:50 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Fri Apr 26 03:36:13 2024 - INFO  - The case exit by code 0.
Fri Apr 26 03:36:14 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Fri Apr 26 03:36:17 2024 - INFO  - start to run testcase:ruyi_test_common.
Fri Apr 26 03:55:15 2024 - INFO  - The case exit by code 0.
Fri Apr 26 03:55:16 2024 - INFO  - End to run testcase:ruyi_test_common.
Fri Apr 26 03:55:18 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Fri Apr 26 04:53:08 2024 - INFO  - The case exit by code 0.
Fri Apr 26 04:53:09 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Fri Apr 26 04:53:11 2024 - INFO  - start to run testcase:ruyi_test_news.
Fri Apr 26 04:56:15 2024 - INFO  - The case exit by code 0.
Fri Apr 26 04:56:16 2024 - INFO  - End to run testcase:ruyi_test_news.
Fri Apr 26 04:56:17 2024 - INFO  - A total of 14 use cases were executed, with 13 successes and 1 failures.
```

### 测试结果

共测试了 1 个测试套， 14 个测试用例，其中

+ 14 个测试用例成功
+ 0 个测试用例失败
+ 0 个测试用例超时

### 测试结论

没有发现问题。

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240423/logs/openkylin_riscv64_qemu/)

