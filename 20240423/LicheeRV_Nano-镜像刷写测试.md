# RUYI 包管理 20240423 版本 LicheeRV Nano 镜像刷写测试报告

本次测试基于 RUYI 20240423 版本预编译的 amd64 架构版本二进制 [ruyi.amd64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.9.0/ruyi.amd64)。

由于涉及到硬盘镜像写入和开发板镜像烧录，本次测试手动进行。

### 测试环境说明

+ 测试环境为 Ubuntu 22.04.4 LTS x86_64。

### 测试执行

0. 安装 ruyi CLI: `sudo curl -Lo /usr/bin/ruyi https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.9.0/ruyi.amd64 && sudo chmod +x /usr/bin/ruyi`

1.  运行 `ruyi device provision`。

2. 按提示进行操作。分别测试 BuildRoot 镜像和 Debian 镜像。

3. 查看镜像是否正确写入，检查开发板能否启动。

### 测试结果

镜像刷写成功。两个系统均可成功启动。

### 测试结论

ruyi 20240423 版本的 `device provision` 功能通过测试。

### 测试用例列表

|                测试用例名                 |          测试内容          |
|:-------------------------------------:|:----------------------:|
| LicheeRV Nano BuildRoot/FreeRTOS 镜像刷写 | 测试镜像刷写及启动是否正常 |
|       LicheeRV Nano Debian 镜像刷写       | 测试镜像刷写及启动是否正常 |


### 失败用例列表

|       测试环境        |     测试用例名      | 状态 |     日志文件      |     原因     |
|:---------------------:|:---------------:|:--:|:----------------:|:----------:|


### 其他信息

屏幕录像：

- BuildRoot

[![asciicast](https://asciinema.org/a/jsYqed2lTOrx36yxkT1M4LPJs.svg)](https://asciinema.org/a/jsYqed2lTOrx36yxkT1M4LPJs)

- Debian

[![asciicast](https://asciinema.org/a/NIfUFT5RSB2MN5EohGLEfkyl3.svg)](https://asciinema.org/a/NIfUFT5RSB2MN5EohGLEfkyl3)