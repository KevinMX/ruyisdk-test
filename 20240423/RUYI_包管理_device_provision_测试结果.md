# RUYI 包管理 device provision 测试结果

本次测试基于 RUYI 0.9.0 版本预编译的 amd64 架构版本二进制 [ruyi.amd64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.9.0/ruyi.amd64) 。

## 测试日志

```
$ bash ruyi_mugen.sh -f ruyismoke -x
Thu Apr 25 19:49:51 2024 - INFO  - start to run testcase:ruyi_smoke_device_provision.
Thu Apr 25 19:49:58 2024 - INFO  - The case exit by code 0.
Thu Apr 25 19:49:58 2024 - INFO  - End to run testcase:ruyi_smoke_device_provision.
Thu Apr 25 19:49:58 2024 - INFO  - A total of 1 use cases were executed, with 1 successes and 0 failures.
```

