# RUYI 包管理 QEMU Fedora38 riscv64 测试结果
本次测试基于 RUYI 0.9.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.9.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 QEMU Fedora38 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ Fedora38 镜像使用 koji 上的 20230825 开发者每日构建镜像 [Fedora-Developer-38-20230825](http://fedora.riscv.rocks/kojifiles/work/tasks/5889/1465889/Fedora-Developer-38-20230825.n.0-sda.raw.xz)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Wed Apr 24 22:48:32 2024 - INFO  - start to run testcase:ruyi_test_device.
Wed Apr 24 23:02:55 2024 - INFO  - The case exit by code 0.
Wed Apr 24 23:03:00 2024 - INFO  - End to run testcase:ruyi_test_device.
Wed Apr 24 23:03:05 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Wed Apr 24 23:54:51 2024 - INFO  - The case exit by code 0.
Wed Apr 24 23:54:52 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Wed Apr 24 23:54:58 2024 - INFO  - start to run testcase:ruyi_test_admin.
Wed Apr 24 23:59:43 2024 - INFO  - The case exit by code 0.
Wed Apr 24 23:59:45 2024 - INFO  - End to run testcase:ruyi_test_admin.
Wed Apr 24 23:59:50 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Thu Apr 25 00:59:55 2024 - WARN  - The case is still running, wait for another 15m.
Thu Apr 25 01:14:57 2024 - WARN  - The case is still running, wait for another 15m.
Thu Apr 25 01:29:59 2024 - WARN  - The case execution timeout.
Thu Apr 25 01:30:01 2024 - ERROR - The case exit by code 143.
Thu Apr 25 01:30:04 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Thu Apr 25 01:30:08 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Thu Apr 25 01:46:35 2024 - INFO  - The case exit by code 0.
Thu Apr 25 01:46:37 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Thu Apr 25 01:46:43 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Thu Apr 25 01:57:51 2024 - ERROR - The case exit by code 1.
Thu Apr 25 01:57:53 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Thu Apr 25 01:57:59 2024 - INFO  - start to run testcase:ruyi_test_common.
Thu Apr 25 02:12:07 2024 - INFO  - The case exit by code 0.
Thu Apr 25 02:12:09 2024 - INFO  - End to run testcase:ruyi_test_common.
Thu Apr 25 02:12:13 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Thu Apr 25 02:15:58 2024 - INFO  - The case exit by code 0.
Thu Apr 25 02:16:00 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Thu Apr 25 02:16:05 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu Apr 25 02:21:11 2024 - INFO  - The case exit by code 0.
Thu Apr 25 02:21:13 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu Apr 25 02:21:18 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Thu Apr 25 02:24:34 2024 - INFO  - The case exit by code 0.
Thu Apr 25 02:24:36 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Thu Apr 25 02:24:41 2024 - INFO  - start to run testcase:ruyi_test_config.
Thu Apr 25 02:28:25 2024 - INFO  - The case exit by code 0.
Thu Apr 25 02:28:27 2024 - INFO  - End to run testcase:ruyi_test_config.
Thu Apr 25 02:28:32 2024 - INFO  - start to run testcase:ruyi_test_venv.
Thu Apr 25 02:40:50 2024 - INFO  - The case exit by code 0.
Thu Apr 25 02:40:52 2024 - INFO  - End to run testcase:ruyi_test_venv.
Thu Apr 25 02:40:56 2024 - INFO  - start to run testcase:ruyi_test_news.
Thu Apr 25 02:45:29 2024 - INFO  - The case exit by code 0.
Thu Apr 25 02:45:31 2024 - INFO  - End to run testcase:ruyi_test_news.
Thu Apr 25 02:45:35 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Thu Apr 25 03:03:07 2024 - INFO  - The case exit by code 0.
Thu Apr 25 03:03:09 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Thu Apr 25 03:03:11 2024 - INFO  - A total of 14 use cases were executed, with 12 successes and 2 failures.
```

### 测试结果

共测试了 1 个测试套， 14 个测试用例，其中

+ 12 个测试用例成功
+ 2 个测试用例失败
+ 1 个测试用例超时

### 测试结论

失败用例经过分析认为非质量问题。

测试没有发现质量问题。

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240423/logs/jenkins/)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240423/logs/fedora38-riscv64-qemu/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240423/logs/jenkins/0.9.0_test.mp4)
