# ruyi 包管理 20240423 版本 测试报告

本次测试基于 `ruyi` [0.9.0](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.9.0/ruyi.riscv64) 版本预编译的 riscv64 架构版本二进制 `ruyi.riscv64`。

编写了 mugen 测试用例，在 openKylin RISC-V v1.0.1 (StarFive VisionFive 2 4GB) 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 `expect`、`psmisc`、`ping`、`make`、`python3`、`python3-paramiko`、`python3-six`，故 `mugen` 自动化测试不能测试 `ruyi` 对这些软件包的依赖。
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的。
+ 测试环境为 openKylin RISC-V v1.0.1。

### 测试执行

0. 以 `openkylin` 用户身份登录系统。

1. 执行自动化测试脚本：`curl -LO https://github.com/KevinMX/PLCT-Tarsier-Works/raw/main/misc/scripts/mugen_ruyi.sh && bash mugen_ruyi.sh openEuler12#$`

### 测试结果

共测试了 1 个测试套，14 个测试用例，14 个测试用例成功，0 个测试用例失败。

### 测试用例列表

| 测试套/软件包名 |             测试用例名             |                 测试内容                  |
|:---------------:|:----------------------------------:|:-------------------------------------:|
|      ruyi       |         ruyi\_test\_common         |               基本命令测试                |
|                 |          ruyi\_test\_xdg           |        ``XDG_*_HOME`` 环境变量测试        |
|                 |          ruyi\_test\_venv          |             ``venv`` 命令测试             |
|                 |         ruyi\_test\_admin          |            ``admin`` 命令测试             |
|                 |      ruyi\_test\_cmake\_ninja      | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|                 |          ruyi\_test\_qemu          |               QEMU 支持测试               |
|                 |      ruyi\_test\_xthead\_qemu      |           平头哥 QEMU 支持测试            |
|                 |          ruyi\_test\_llvm          |               LLVM 支持测试               |
|                 |          ruyi\_test\_news          |             ``news`` 命令测试             |
|                 |         ruyi\_test\_device         |            ``device`` 命令测试            |
|                 | ruyi\_test\_gnu-plct-rv64ilp32-elf |   ``gnu-plct-rv64ilp32-elf`` 工具链测试   |
|                 |         ruyi\_test\_config         |         config.toml 配置文件测试          |
|                 |        ruyi\_test\_binaries        |               二进制包测试                |
|                 | ruyi_test_gnu-plct_xiangshan-nanhu |    `gnu-plct` 工具链/香山南湖平台测试     |

### 其他信息

详细日志见 [openkylin-riscv64-visionfive2](/20240423/logs/openkylin-riscv64-visionfive2/) 目录。

#### 0.9.0
```log
Sun Apr 28 05:48:03 2024 - INFO  - 配置文件加载完成...
Sun Apr 28 05:48:04 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Sun Apr 28 05:49:01 2024 - INFO  - The case exit by code 0.
Sun Apr 28 05:49:01 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Sun Apr 28 05:49:02 2024 - INFO  - start to run testcase:ruyi_test_device.
Sun Apr 28 05:50:07 2024 - INFO  - The case exit by code 0.
Sun Apr 28 05:50:07 2024 - INFO  - End to run testcase:ruyi_test_device.
Sun Apr 28 05:50:08 2024 - INFO  - start to run testcase:ruyi_test_news.
Sun Apr 28 05:51:16 2024 - INFO  - The case exit by code 0.
Sun Apr 28 05:51:16 2024 - INFO  - End to run testcase:ruyi_test_news.
Sun Apr 28 05:51:17 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Sun Apr 28 05:59:56 2024 - INFO  - The case exit by code 0.
Sun Apr 28 05:59:56 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Sun Apr 28 05:59:57 2024 - INFO  - start to run testcase:ruyi_test_config.
Sun Apr 28 06:01:00 2024 - INFO  - The case exit by code 0.
Sun Apr 28 06:01:00 2024 - INFO  - End to run testcase:ruyi_test_config.
Sun Apr 28 06:01:00 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Sun Apr 28 06:18:30 2024 - INFO  - The case exit by code 0.
Sun Apr 28 06:18:30 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Sun Apr 28 06:18:30 2024 - INFO  - start to run testcase:ruyi_test_venv.
Sun Apr 28 06:24:45 2024 - INFO  - The case exit by code 0.
Sun Apr 28 06:24:45 2024 - INFO  - End to run testcase:ruyi_test_venv.
Sun Apr 28 06:24:45 2024 - INFO  - start to run testcase:ruyi_test_common.
Sun Apr 28 06:32:06 2024 - INFO  - The case exit by code 0.
Sun Apr 28 06:32:06 2024 - INFO  - End to run testcase:ruyi_test_common.
Sun Apr 28 06:32:07 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Sun Apr 28 06:33:57 2024 - INFO  - The case exit by code 0.
Sun Apr 28 06:33:58 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Sun Apr 28 06:33:58 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Sun Apr 28 06:34:54 2024 - INFO  - The case exit by code 0.
Sun Apr 28 06:34:54 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Sun Apr 28 06:34:55 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Sun Apr 28 06:41:07 2024 - INFO  - The case exit by code 0.
Sun Apr 28 06:41:07 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Sun Apr 28 06:41:07 2024 - INFO  - start to run testcase:ruyi_test_admin.
Sun Apr 28 06:42:05 2024 - INFO  - The case exit by code 0.
Sun Apr 28 06:42:05 2024 - INFO  - End to run testcase:ruyi_test_admin.
Sun Apr 28 06:42:05 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Sun Apr 28 07:06:44 2024 - INFO  - The case exit by code 0.
Sun Apr 28 07:06:44 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Sun Apr 28 07:06:44 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Sun Apr 28 07:12:53 2024 - INFO  - The case exit by code 0.
Sun Apr 28 07:12:53 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Sun Apr 28 07:12:53 2024 - INFO  - A total of 14 use cases were executed, with 14 successes and 0 failures.
Processing logs...
openkylin@openkylin:~$ 
```