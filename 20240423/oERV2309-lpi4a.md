# ruyi 包管理 20240423 版本 测试报告

本次测试基于 `ruyi` [0.9.0](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.9.0/ruyi.riscv64) 版本预编译的 riscv64 架构版本二进制 `ruyi.riscv64`。

编写了 mugen 测试用例，在 openEuler 23.09 RISC-V preview V1 (Lichee Pi 4A 16G) 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 `expect`、`psmisc`、`ping`、`make`、`python3`、`python3-paramiko`、`python3-six`，故 `mugen` 自动化测试不能测试 `ruyi` 对这些软件包的依赖。
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的。
+ 测试环境为 openEuler 23.09 RISC-V preview V1。

### 测试执行

0. 以 `openeuler` 用户身份登录系统。

1. 执行自动化测试脚本：`curl -LO https://github.com/KevinMX/PLCT-Tarsier-Works/raw/main/misc/scripts/mugen_ruyi.sh && bash mugen_ruyi.sh openEuler12#$`

### 测试结果

共测试了 1 个测试套，14 个测试用例，14 个测试用例成功，0 个测试用例失败。

### 测试用例列表

| 测试套/软件包名 |             测试用例名             |                 测试内容                  |
|:---------------:|:----------------------------------:|:-------------------------------------:|
|      ruyi       |         ruyi\_test\_common         |               基本命令测试                |
|                 |          ruyi\_test\_xdg           |        ``XDG_*_HOME`` 环境变量测试        |
|                 |          ruyi\_test\_venv          |             ``venv`` 命令测试             |
|                 |         ruyi\_test\_admin          |            ``admin`` 命令测试             |
|                 |      ruyi\_test\_cmake\_ninja      | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|                 |          ruyi\_test\_qemu          |               QEMU 支持测试               |
|                 |      ruyi\_test\_xthead\_qemu      |           平头哥 QEMU 支持测试            |
|                 |          ruyi\_test\_llvm          |               LLVM 支持测试               |
|                 |          ruyi\_test\_news          |             ``news`` 命令测试             |
|                 |         ruyi\_test\_device         |            ``device`` 命令测试            |
|                 | ruyi\_test\_gnu-plct-rv64ilp32-elf |   ``gnu-plct-rv64ilp32-elf`` 工具链测试   |
|                 |         ruyi\_test\_config         |         config.toml 配置文件测试          |
|                 |        ruyi\_test\_binaries        |               二进制包测试                |
|                 | ruyi_test_gnu-plct_xiangshan-nanhu |    `gnu-plct` 工具链/香山南湖平台测试     |

### 其他信息

详细日志见 [oE2309-riscv64-lpi4a](/20240423/logs/oE2309-riscv64-lpi4a/) 目录。

#### 0.9.0

```log
[openeuler@openeuler-riscv64 ~]$ curl -LO https://raw.githubusercontent.com/KevinMX/PLCT-Tarsier-Works/main/misc/scripts/mugen_ruyi.sh && chmod +x mugen_ruyi.sh && bash mugen_ruyi.sh openEuler12#$                                                                                                        
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current                                                                       
                                 Dload  Upload   Total   Spent    Left  Speed                                                                         
100  1967  100  1967    0     0    490      0  0:00:04  0:00:04 --:--:--   491                                                                        
Last metadata expiration check: 0:03:32 ago on Tue 23 Apr 2024 02:00:00 PM CST.                                                                       
Package git-2.41.0-1.oe2309.riscv64 is already installed.                                                                                             
Dependencies resolved.                                                     
Nothing to do.
Complete!
Cloning into 'ruyi-mugen'...
remote: Enumerating objects: 156, done.
remote: Counting objects: 100% (156/156), done.
remote: Compressing objects: 100% (144/144), done.
remote: Total 156 (delta 23), reused 102 (delta 4), pack-reused 0
Receiving objects: 100% (156/156), 2.67 MiB | 233.00 KiB/s, done.
Resolving deltas: 100% (23/23), done.
sudo: apt-get: command not found
Not apt distro
Last metadata expiration check: 0:03:49 ago on Tue 23 Apr 2024 02:00:00 PM CST.
Dependencies resolved.
Nothing to do.
Complete!
Last metadata expiration check: 0:03:55 ago on Tue 23 Apr 2024 02:00:00 PM CST.
Package sudo-1.9.14p1-1.oe2309.riscv64 is already installed.
Package expect-1:5.45.4-7.oe2309.riscv64 is already installed.
Package psmisc-23.6-1.oe2309.riscv64 is already installed.
Package make-1:4.4.1-1.oe2309.riscv64 is already installed.
Package iputils-20221126-2.oe2309.riscv64 is already installed.
Package python3-six-1.16.0-3.oe2309.noarch is already installed.
Package python3-paramiko-3.2.0-1.oe2309.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!
sudo: pacman: command not found
Not archlinux distro
sudo: emerge-webrsync: command not found
Not gentoo linux
Tue Apr 23 14:03:59 2024 - INFO  - 配置文件加载完成...
Tue Apr 23 14:04:00 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Tue Apr 23 14:06:30 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:06:30 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Tue Apr 23 14:06:31 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Tue Apr 23 14:07:16 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:07:16 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Tue Apr 23 14:07:16 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Tue Apr 23 14:08:37 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:08:37 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Tue Apr 23 14:08:37 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Tue Apr 23 14:17:33 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:17:33 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Tue Apr 23 14:17:34 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Tue Apr 23 14:22:54 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:22:55 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Tue Apr 23 14:22:55 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Tue Apr 23 14:24:42 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:24:42 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Tue Apr 23 14:24:42 2024 - INFO  - start to run testcase:ruyi_test_config.
Tue Apr 23 14:25:08 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:25:08 2024 - INFO  - End to run testcase:ruyi_test_config.
Tue Apr 23 14:25:08 2024 - INFO  - start to run testcase:ruyi_test_admin.
Tue Apr 23 14:25:30 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:25:31 2024 - INFO  - End to run testcase:ruyi_test_admin.
Tue Apr 23 14:25:31 2024 - INFO  - start to run testcase:ruyi_test_news.
Tue Apr 23 14:26:02 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:26:03 2024 - INFO  - End to run testcase:ruyi_test_news.
Tue Apr 23 14:26:03 2024 - INFO  - start to run testcase:ruyi_test_device.
Tue Apr 23 14:26:32 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:26:32 2024 - INFO  - End to run testcase:ruyi_test_device.
Tue Apr 23 14:26:32 2024 - INFO  - start to run testcase:ruyi_test_common.
Tue Apr 23 14:28:30 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:28:30 2024 - INFO  - End to run testcase:ruyi_test_common.
Tue Apr 23 14:28:31 2024 - INFO  - start to run testcase:ruyi_test_venv.
Tue Apr 23 14:30:22 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:30:22 2024 - INFO  - End to run testcase:ruyi_test_venv.
Tue Apr 23 14:30:22 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Tue Apr 23 14:30:43 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:30:43 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Tue Apr 23 14:30:43 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Tue Apr 23 14:31:04 2024 - INFO  - The case exit by code 0.
Tue Apr 23 14:31:04 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Tue Apr 23 14:31:04 2024 - INFO  - A total of 14 use cases were executed, with 14 successes and 0 failures.
Processing logs...
[openeuler@openeuler-riscv64 ~]$
```