# RUYI 包管理 QEMU Ubuntu 22.04 riscv64 测试结果

本次测试基于 RUYI 0.9.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.9.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 QEMU ubuntu 22.04 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect、 psmisc、 ping、 make、 python3、 python3-paramiko、 python3-six，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ Ubuntu 22.04 LTS 镜像使用 Ubuntu 提供的 Unmatched 镜像 [ubuntu-22.04.4-preinstalled-server-riscv64\_unmatched](https://mirrors.bfsu.edu.cn/ubuntu-cdimage/releases/22.04.4/release/ubuntu-22.04.4-preinstalled-server-riscv64%2Bunmatched.img.xz)
+ 在官方文档中 QEMU 与 Unmatched 共用一个镜像

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Thu Apr 25 06:03:18 2024 - INFO  - start to run testcase:ruyi_test_admin.
Thu Apr 25 06:14:24 2024 - INFO  - The case exit by code 0.
Thu Apr 25 06:14:25 2024 - INFO  - End to run testcase:ruyi_test_admin.
Thu Apr 25 06:14:29 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Thu Apr 25 06:38:23 2024 - INFO  - The case exit by code 0.
Thu Apr 25 06:38:24 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Thu Apr 25 06:38:28 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Thu Apr 25 07:05:47 2024 - INFO  - The case exit by code 0.
Thu Apr 25 07:05:49 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Thu Apr 25 07:05:53 2024 - INFO  - start to run testcase:ruyi_test_binaries.
Thu Apr 25 07:44:48 2024 - INFO  - The case exit by code 0.
Thu Apr 25 07:44:49 2024 - INFO  - End to run testcase:ruyi_test_binaries.
Thu Apr 25 07:44:53 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu Apr 25 07:55:11 2024 - INFO  - The case exit by code 0.
Thu Apr 25 07:55:12 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct-rv64ilp32-elf.
Thu Apr 25 07:55:16 2024 - INFO  - start to run testcase:ruyi_test_common.
Thu Apr 25 08:19:31 2024 - INFO  - The case exit by code 0.
Thu Apr 25 08:19:33 2024 - INFO  - End to run testcase:ruyi_test_common.
Thu Apr 25 08:19:36 2024 - INFO  - start to run testcase:ruyi_test_config.
Thu Apr 25 08:27:38 2024 - INFO  - The case exit by code 0.
Thu Apr 25 08:27:39 2024 - INFO  - End to run testcase:ruyi_test_config.
Thu Apr 25 08:27:43 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Thu Apr 25 08:33:40 2024 - INFO  - The case exit by code 0.
Thu Apr 25 08:33:42 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Thu Apr 25 08:33:45 2024 - INFO  - start to run testcase:ruyi_test_news.
Thu Apr 25 08:40:25 2024 - INFO  - The case exit by code 0.
Thu Apr 25 08:40:27 2024 - INFO  - End to run testcase:ruyi_test_news.
Thu Apr 25 08:40:30 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Thu Apr 25 08:46:37 2024 - INFO  - The case exit by code 0.
Thu Apr 25 08:46:39 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Thu Apr 25 08:46:43 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Thu Apr 25 09:36:07 2024 - INFO  - The case exit by code 0.
Thu Apr 25 09:36:08 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Thu Apr 25 09:36:12 2024 - INFO  - start to run testcase:ruyi_test_venv.
Thu Apr 25 09:49:30 2024 - INFO  - The case exit by code 0.
Thu Apr 25 09:49:31 2024 - INFO  - End to run testcase:ruyi_test_venv.
Thu Apr 25 09:49:35 2024 - INFO  - start to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Thu Apr 25 10:03:33 2024 - INFO  - The case exit by code 0.
Thu Apr 25 10:03:35 2024 - INFO  - End to run testcase:ruyi_test_gnu-plct_xiangshan-nanhu.
Thu Apr 25 10:03:39 2024 - INFO  - start to run testcase:ruyi_test_device.
Thu Apr 25 10:10:38 2024 - INFO  - The case exit by code 0.
Thu Apr 25 10:10:40 2024 - INFO  - End to run testcase:ruyi_test_device.
Thu Apr 25 10:10:41 2024 - INFO  - A total of 14 use cases were executed, with 14 successes and 0 failures.
```

### 测试结果

共测试了 1 个测试套， 14 个测试用例，其中

+ 14 个测试用例成功
+ 0 个测试用例失败
+ 0 个测试用例超时

### 测试结论

没有发现问题

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |
|  | ruyi\_test\_gnu-plct-rv64ilp32-elf | ``gnu-plct-rv64ilp32-elf`` 工具链测试 |
|  | ruyi\_test\_config | config.toml 配置文件测试 |
|  | ruyi\_test\_binaries | 二进制包测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240423/logs/jenkins/)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240423/logs/ubuntu2204-riscv64-qemu/)
+ 视频录像 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240423/logs/jenkins/0.9.0_test.mp4)
