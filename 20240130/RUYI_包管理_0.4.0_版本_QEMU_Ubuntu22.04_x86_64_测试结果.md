# RUYI 包管理 0.4.0 版本 QEMU Ubuntu22.04 x86_64 测试结果

本次测试基于 RUYI 0.4.0 版本预编译的 amd64 架构版本二进制 [ruyi.amd64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.4.0/ruyi.amd64) 。

编写了 mugen 测试用例，在 QEMU ubuntu 22.04 x86_64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect 、 psmisc 、 ping 、 make 、 python3 、 python3-paramiko 、 python3-six ，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ Ubuntu 22.04 LTS 镜像使用 Ubuntu 提供的 QEMU 镜像 [ubuntu-22.04-server-cloudimg-amd64](https://cloud-images.ubuntu.com/releases/jammy/release/ubuntu-22.04-server-cloudimg-amd64-disk-kvm.img)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Sat Feb  3 12:10:43 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Sat Feb  3 12:16:41 2024 - INFO  - The case exit by code 0.
Sat Feb  3 12:16:41 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Sat Feb  3 12:16:41 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Sat Feb  3 12:49:46 2024 - INFO  - The case exit by code 0.
Sat Feb  3 12:49:46 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Sat Feb  3 12:49:46 2024 - INFO  - start to run testcase:ruyi_test_venv.
Sat Feb  3 13:12:38 2024 - INFO  - The case exit by code 0.
Sat Feb  3 13:12:38 2024 - INFO  - End to run testcase:ruyi_test_venv.
Sat Feb  3 13:12:38 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Sat Feb  3 13:51:14 2024 - INFO  - The case exit by code 0.
Sat Feb  3 13:51:14 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Sat Feb  3 13:51:14 2024 - INFO  - start to run testcase:ruyi_test_admin.
Sat Feb  3 13:52:39 2024 - INFO  - The case exit by code 0.
Sat Feb  3 13:52:39 2024 - INFO  - End to run testcase:ruyi_test_admin.
Sat Feb  3 13:52:39 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Sat Feb  3 14:09:48 2024 - INFO  - The case exit by code 0.
Sat Feb  3 14:09:48 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Sat Feb  3 14:09:48 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Sat Feb  3 14:26:23 2024 - INFO  - The case exit by code 0.
Sat Feb  3 14:26:23 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Sat Feb  3 14:26:23 2024 - INFO  - start to run testcase:ruyi_test_common.
Sat Feb  3 14:39:27 2024 - INFO  - The case exit by code 0.
Sat Feb  3 14:39:27 2024 - INFO  - End to run testcase:ruyi_test_common.
Sat Feb  3 14:39:27 2024 - INFO  - start to run testcase:ruyi_test_device.
Sat Feb  3 15:06:50 2024 - INFO  - The case exit by code 0.
Sat Feb  3 15:06:50 2024 - INFO  - End to run testcase:ruyi_test_device.
Sat Feb  3 15:06:50 2024 - INFO  - start to run testcase:ruyi_test_news.
Sat Feb  3 15:08:00 2024 - INFO  - The case exit by code 0.
Sat Feb  3 15:08:01 2024 - INFO  - End to run testcase:ruyi_test_news.
Sat Feb  3 15:08:01 2024 - INFO  - A total of 10 use cases were executed, with 10 successes and 0 failures.
```

### 测试结果

共测试了 1 个测试套， 10 个测试用例，其中

+ 10 个测试用例成功

### 其他信息

在下载大文件遇到某些错误时，下载会终止而不会重试，关联 issue [#64](https://github.com/ruyisdk/ruyi/issues/64)

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240130/logs/jenkins.log)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240130/logs/ubuntu2204-x86_64-qemu/)
+ 视频录像，由于测试总时长达 4 小时，视频只演示测试触发和测试产物 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240116/videos/gitee-jenkins-test.mp4)

