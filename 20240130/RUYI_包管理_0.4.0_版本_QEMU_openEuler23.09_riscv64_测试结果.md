# RUYI 包管理 0.4.0 版本 QEMU openEuler 23.09 riscv64 测试结果

本次测试基于 RUYI 0.4.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.4.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 QEMU openEuler 23.09 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect 、 psmisc 、 ping 、 make 、 python3 、 python3-paramiko 、 python3-six ，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ openEuler 2309 riscv64 镜像使用软件所提供的镜像 [openEuler 2309 riscv64 base](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/20231130/v0.2/QEMU/openEuler-23.09-V1-base-qemu-testing.qcow2.zst)

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Sun Feb  4 02:15:24 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Sun Feb  4 02:17:00 2024 - INFO  - The case exit by code 0.
Sun Feb  4 02:17:02 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Sun Feb  4 02:17:07 2024 - INFO  - start to run testcase:ruyi_test_device.
Sun Feb  4 03:00:31 2024 - INFO  - The case exit by code 0.
Sun Feb  4 03:00:33 2024 - INFO  - End to run testcase:ruyi_test_device.
Sun Feb  4 03:00:37 2024 - INFO  - start to run testcase:ruyi_test_news.
Sun Feb  4 03:04:24 2024 - INFO  - The case exit by code 0.
Sun Feb  4 03:04:25 2024 - INFO  - End to run testcase:ruyi_test_news.
Sun Feb  4 03:04:28 2024 - INFO  - start to run testcase:ruyi_test_venv.
Sun Feb  4 03:41:01 2024 - INFO  - The case exit by code 0.
Sun Feb  4 03:41:02 2024 - INFO  - End to run testcase:ruyi_test_venv.
Sun Feb  4 03:41:06 2024 - INFO  - start to run testcase:ruyi_test_common.
Sun Feb  4 04:11:19 2024 - INFO  - The case exit by code 0.
Sun Feb  4 04:11:20 2024 - INFO  - End to run testcase:ruyi_test_common.
Sun Feb  4 04:11:23 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Sun Feb  4 05:11:28 2024 - WARN  - The case execution timeout.
Sun Feb  4 05:11:29 2024 - ERROR - The case exit by code 143.
Sun Feb  4 05:11:31 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Sun Feb  4 05:11:34 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Sun Feb  4 05:12:50 2024 - INFO  - The case exit by code 0.
Sun Feb  4 05:12:51 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Sun Feb  4 05:12:54 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Sun Feb  4 05:14:28 2024 - ERROR - The case exit by code 11.
Sun Feb  4 05:14:30 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Sun Feb  4 05:14:32 2024 - INFO  - start to run testcase:ruyi_test_admin.
Sun Feb  4 05:15:47 2024 - INFO  - The case exit by code 0.
Sun Feb  4 05:15:49 2024 - INFO  - End to run testcase:ruyi_test_admin.
Sun Feb  4 05:15:52 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Sun Feb  4 05:17:37 2024 - INFO  - The case exit by code 0.
Sun Feb  4 05:17:38 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Sun Feb  4 05:17:39 2024 - INFO  - A total of 10 use cases were executed, with 8 successes and 2 failures.
```

重测失败用例

```bash
Fri Feb  2 12:07:01 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Fri Feb  2 13:07:05 2024 - WARN  - The case execution timeout.
Fri Feb  2 13:07:06 2024 - ERROR - The case exit by code 143.
Fri Feb  2 13:07:07 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Fri Feb  2 14:03:41 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Fri Feb  2 14:27:54 2024 - INFO  - The case exit by code 0.
Fri Feb  2 14:27:55 2024 - INFO  - End to run testcase:ruyi_test_xdg.
```

### 测试结果

共测试了 1 个测试套， 10 个测试用例，其中

+ 10 个测试用例成功

由于 ISCAS 镜像站的问题，其中 ruyi\_test\_xdg 与 ruyi\_test\_cmake\_ninja 是单独测试的；尽管 cmake\_ninja 测试超时，但是测试整体看是成功的。

### 其他信息

在下载大文件遇到某些错误时，下载会终止而不会重试，关联 issue [#64](https://github.com/ruyisdk/ruyi/issues/64)

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240130/logs/jenkins.log)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240130/logs/oE2309-riscv64-qemu/)
+ 视频录像，由于测试总时长达 4 小时，视频只演示测试触发和测试产物 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240116/videos/gitee-jenkins-test.mp4)

