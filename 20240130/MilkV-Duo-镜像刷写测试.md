# RUYI 包管理 20240130 版本镜像刷写测试报告

本次测试基于 RUYI 20240130 版本预编译的 riscv64 架构版本二进制 ruyi.riscv64。

由于涉及到硬盘镜像写入和开发板镜像烧录，本次测试手动进行。

### 测试环境说明

+ 测试环境为 Arch Linux x86_64。

### 测试执行

0. 安装 ruyi CLI: `sudo curl -Lo /usr/bin/ruyi https://mirror.iscas.ac.cn/ruyisdk/ruyi/testing/ruyi.amd64.20240116 && sudo chmod +x /usr/bin/ruyi`
2. 运行 `ruyi device provition`。
3. 按提示进行操作。
4. 查看镜像是否正确写入，检查开发板能否启动。

### 测试结果

Milk-V Duo 64M 版本的镜像能够被正常写入且正常启动；

镜像能被正确写入到外置存储设备上，经单独测试，系统正常启动。

### 测试结论

ruyi 20240130 版本的 device provision 功能通过测试。

### 测试用例列表

|          测试用例名          |            测试内容            |
|:------------------------:|:--------------------------:|
| Milk-V Duo 镜像刷写 |      测试 dd 镜像是否正常      |


### 失败用例列表

| 测试环境 | 测试用例名 | 状态 | 日志文件 | 原因 |
| :-: | :-: | :-: | :-: | :-: |

### 其他信息

屏幕录像可查看：

#### Milk-V Duo
[![Milk-V Duo](https://asciinema.org/a/rsenSOJwdlmUXcJ8sQwubPgtr.svg)](https://asciinema.org/a/rsenSOJwdlmUXcJ8sQwubPgtr?autoplay=1)

