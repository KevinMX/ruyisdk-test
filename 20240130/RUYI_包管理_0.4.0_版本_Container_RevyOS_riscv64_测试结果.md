# RUYI 包管理 0.4.0 版本 Container RevyOS riscv64 测试结果

本次测试基于 RUYI 0.4.0 版本预编译的 riscv64 架构版本二进制 [ruyi.riscv64](https://mirror.iscas.ac.cn/ruyisdk/ruyi/releases/0.4.0/ruyi.riscv64) 。

编写了 mugen 测试用例，在 systemd-nspawn RevyOS 20231210 riscv64 开展测试。

## mugen 测试

### 测试环境说明

+ 由于 mugen 本身依赖 expect 、 psmisc 、 ping 、 make 、 python3 、 python3-paramiko 、 python3-six ，故 mugen 自动化测试不能测试 RUYI 对这些软件包的依赖
+ 由于 mugen 自身的特性，测试用例运行的顺序是随机的
+ RevyOS 镜像使用当前最新版本 [20231210](https://mirror.iscas.ac.cn/revyos/extra/images/lpi4a/20231210/)
+ RevyOS 容器测试环境为在 RevyOS 20231210 上搭建的 systemd-nspawn 容器，预装软件与 20231210 镜像看齐

### 测试流程

+ 由 Jenkins CI Pipeline 自动触发测试

### 测试日志

```bash
+ sudo bash mugen.sh -f ruyi -x
Fri Feb  2 03:19:50 2024 - INFO  - start to run testcase:ruyi_test_device.
Fri Feb  2 03:49:14 2024 - INFO  - The case exit by code 0.
Fri Feb  2 03:49:15 2024 - INFO  - End to run testcase:ruyi_test_device.
Fri Feb  2 03:49:15 2024 - INFO  - start to run testcase:ruyi_test_admin.
Fri Feb  2 03:50:09 2024 - INFO  - The case exit by code 0.
Fri Feb  2 03:50:09 2024 - INFO  - End to run testcase:ruyi_test_admin.
Fri Feb  2 03:50:09 2024 - INFO  - start to run testcase:ruyi_test_common.
Fri Feb  2 04:05:34 2024 - INFO  - The case exit by code 0.
Fri Feb  2 04:05:34 2024 - INFO  - End to run testcase:ruyi_test_common.
Fri Feb  2 04:05:35 2024 - INFO  - start to run testcase:ruyi_test_llvm.
Fri Feb  2 04:06:56 2024 - INFO  - The case exit by code 0.
Fri Feb  2 04:06:56 2024 - INFO  - End to run testcase:ruyi_test_llvm.
Fri Feb  2 04:06:57 2024 - INFO  - start to run testcase:ruyi_test_xthead_qemu.
Fri Feb  2 04:08:03 2024 - INFO  - The case exit by code 0.
Fri Feb  2 04:08:03 2024 - INFO  - End to run testcase:ruyi_test_xthead_qemu.
Fri Feb  2 04:08:03 2024 - INFO  - start to run testcase:ruyi_test_xdg.
Fri Feb  2 04:24:03 2024 - INFO  - The case exit by code 0.
Fri Feb  2 04:24:03 2024 - INFO  - End to run testcase:ruyi_test_xdg.
Fri Feb  2 04:24:03 2024 - INFO  - start to run testcase:ruyi_test_venv.
Fri Feb  2 04:36:42 2024 - INFO  - The case exit by code 0.
Fri Feb  2 04:36:42 2024 - INFO  - End to run testcase:ruyi_test_venv.
Fri Feb  2 04:36:43 2024 - INFO  - start to run testcase:ruyi_test_news.
Fri Feb  2 04:37:53 2024 - INFO  - The case exit by code 0.
Fri Feb  2 04:37:54 2024 - INFO  - End to run testcase:ruyi_test_news.
Fri Feb  2 04:37:54 2024 - INFO  - start to run testcase:ruyi_test_cmake_ninja.
Fri Feb  2 05:05:39 2024 - INFO  - The case exit by code 0.
Fri Feb  2 05:05:39 2024 - INFO  - End to run testcase:ruyi_test_cmake_ninja.
Fri Feb  2 05:05:39 2024 - INFO  - start to run testcase:ruyi_test_qemu.
Fri Feb  2 05:06:46 2024 - INFO  - The case exit by code 0.
Fri Feb  2 05:06:46 2024 - INFO  - End to run testcase:ruyi_test_qemu.
Fri Feb  2 05:06:46 2024 - INFO  - A total of 10 use cases were executed, with 10 successes and 0 failures.
```

### 测试结果

共测试了 1 个测试套， 10 个测试用例，其中

+ 10 个测试用例成功

### 测试结论

没有发现问题

### 测试用例列表

| 测试套/软件包名 | 测试用例名 | 测试内容 |
| :-: | :-: | :-: |
| ruyi | ruyi\_test\_common | 基本命令测试 |
|  | ruyi\_test\_xdg | ``XDG_*_HOME`` 环境变量测试 |
|  | ruyi\_test\_venv | ``venv`` 命令测试 |
|  | ruyi\_test\_admin | ``admin`` 命令测试 |
|  | ruyi\_test\_cmake\_ninja | ``make`` 、 ``cmake`` 、 ``ninja`` 构建测试 |
|  | ruyi\_test\_qemu | QEMU 支持测试 |
|  | ruyi\_test\_xthead\_qemu | 平头哥 QEMU 支持测试 |
|  | ruyi\_test\_llvm | LLVM 支持测试 |
|  | ruyi\_test\_news | ``news`` 命令测试 |
|  | ruyi\_test\_device | ``device`` 命令测试 |

### 其他信息

+ Jenkins CI [日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240130/logs/jenkins.log)
+ 完整测试[日志](https://gitee.com/yunxiangluo/ruyisdk-test/tree/master/20240130/logs/revyos_riscv64_container/)
+ 视频录像，由于测试总时长达 4 小时，视频只演示测试触发和测试产物 ![](https://gitee.com/yunxiangluo/ruyisdk-test/raw/master/20240116/videos/gitee-jenkins-test.mp4)

